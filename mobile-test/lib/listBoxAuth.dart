import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'UserDetail.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ListBox_auth extends StatefulWidget {
  var item;
  ListBox_auth({Key key, this.item}) : super(key: key) {}

  @override
  _ListBox_auth createState() => _ListBox_auth(item: item);
}

class _ListBox_auth extends State<ListBox_auth> {
  String station_location;
  String station_no;
  String station_id;
  List auth_box = new List();
  var item;
  _ListBox_auth({Key key, this.item}) {
    station_location = item["location"];
    station_no = item["no"];
    station_id = item["_id"];
    auth_box = new List();
    print(UserDetails.auth_store);
    for (var i = 0; i < UserDetails.auth_store.length; i++) {
      if (UserDetails.auth_store[i]['authorize_id']['_id'] ==
              UserDetails.auth_id &&
          UserDetails.auth_store[i]['station_id']['_id'] == station_id) {
        auth_box.add(UserDetails.auth_store[i]['box_id']);
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Location "
            '${station_location}'
            ' - '
            "No "
            '${station_no}'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: <Color>[Colors.blue, Colors.teal[100]],
            ),
          ),
        ),
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: auth_box.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: ListBoxDisplay(item: auth_box[index]),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Box_auth(item: auth_box[index]),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ]),
      bottomNavigationBar: Naviagationbar(),
    );
  }
}

class Box_auth extends StatefulWidget {
  var item;
  Box_auth({Key key, this.item}) : super(key: key);

  @override
  _Box_auth createState() => _Box_auth(item: item);
}

class _Box_auth extends State<Box_auth> {
  var item;
  var box_id;
  var box_no;
  var station_id;

  _Box_auth({Key key, this.item}) {
    box_id = item['_id'];
    box_no = item['no'];
    station_id = item['station_id'];
    print(item);
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("No " '${box_no}'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.blue, Colors.teal[100]],
              ),
            ),
          ),
        ),
        body: Container(
            child: Padding(
                padding: EdgeInsets.only(top: 30.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                          onPressed: () async {
                            Response response = await post(
                                UserDetails.api + 'revoke_a_box',
                                body: {
                                  'authorize_id': UserDetails.auth_id,
                                  'owner_id': UserDetails.id,
                                  'box_id': box_id,
                                  'station_id': station_id,
                                });
                            Fluttertoast.showToast(
                                msg: response.body,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.blue,
                                textColor: Colors.white);
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0)),
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: <Color>[
                                  Color(0xFF0D47A1),
                                  Color(0xFF1976D2),
                                  Color(0xFF42A5F5),
                                ],
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(80.0)),
                            ),
                            padding: const EdgeInsets.all(10.0),
                            child: const Text('Revoke',
                                style: TextStyle(fontSize: 20)),
                          )),
                    ]))),
        bottomNavigationBar: Naviagationbar());
  }
}
