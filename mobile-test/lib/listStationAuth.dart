import 'package:flutter/material.dart';
import 'UserDetail.dart';
import 'listBoxAuth.dart';

class ListStation_auth extends StatefulWidget {
  var item;
  ListStation_auth({Key key, this.item}) : super(key: key);
  @override
  _ListStation_auth createState() => _ListStation_auth(item: item);
}

class _ListStation_auth extends State<ListStation_auth> {
  String auth_id;
  String auth_username;
  String auth_email;
  List auth_station = new List();
  var item;
  _ListStation_auth({Key key, this.item}) {
    auth_id = item['_id'];
    auth_username = item['username'];
    auth_email = item['email'];
    auth_station = new List();
    for (var i = 0; i < UserDetails.auth_store.length; i++) {
      if (UserDetails.auth_store[i]['authorize_id']['_id'] == auth_id) {
        if (auth_station.length == 0) {
          auth_station.add(UserDetails.auth_store[i]['station_id']);
        } else {
          for (var j = 0; j < auth_station.length; j++) {
            var check = false;
            if (auth_station[j]['_id'] ==
                UserDetails.auth_store[i]['station_id']['_id']) check = true;
            if (check == false)
              auth_station.add(UserDetails.auth_store[i]['station_id']);
          }
        }
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(auth_username),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: <Color>[Colors.blue, Colors.teal[100]],
            ),
          ),
        ),
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: auth_station.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: ListStationDisplay(item: auth_station[index]),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              ListBox_auth(item: auth_station[index]),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ]),
      bottomNavigationBar: Naviagationbar(),
    );
  }
}
