import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home_screen.dart';
import 'login_form.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'UserDetail.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyApp createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  bool checkUser;
  static const platform = const MethodChannel('flutter.native/AndroidPlatform');

  @override
  void initState() {
    super.initState();
    this.userState();
    var dmyString = '1 1 1997';
    var dateTime1 = DateFormat('d M yyyy').parse(dmyString);
    print(dateTime1);
  }

  void userState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool id_state = prefs.containsKey("id");
    setState(() {
      checkUser = id_state;
    });
    if (id_state == true) {
      UserDetails.id = prefs.getString('id');
      UserDetails.email = prefs.getString('email');
      UserDetails.username = prefs.getString('username');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DEMO GP1 test',
      home: checkUser == true ? Home_screen() : LoginScreen(),
    );
  }
}
