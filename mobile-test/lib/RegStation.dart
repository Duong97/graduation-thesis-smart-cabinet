import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'UserDetail.dart';
import 'dart:convert';
import 'RegBox.dart';
import 'scaffold_custom.dart';

class RegStation extends StatefulWidget {
  @override
  _RegStation createState() => _RegStation();
}

class _RegStation extends State<RegStation> {
  final TextEditingController _filter = new TextEditingController();
  String _searchText = "";
  List searchData = new List();
  List searchDisplay = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Register');
  var previousInput;

  _RegStation() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    _filter.clear();

    super.initState();
  }

  void _searchPressed() {
    print("hey");
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Register');

        _filter.clear();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        drawer: AppDrawer(),
        appBar: new AppBar(
          title: _appBarTitle,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.blue, Colors.teal[100]],
              ),
            ),
          ),
          actions: <Widget>[
            Container(
                padding: EdgeInsets.all(2.0),
                child: IconButton(
                  onPressed: () async {
                    String qrResult = await UserDetails.Scann_QR();

                    Response response = await post(
                        UserDetails.api + 'query_station_id',
                        body: {'token': qrResult});

                    var result = json.decode(response.body);

                    if (result['station_id'] != null) {
                      //print(UserDetails.listStation);
                      var check = false;
                      var index;
                      for (var i = 0; i < UserDetails.regStation.length; i++) {
                        if (UserDetails.regStation[i]['_id'] ==
                            result['station_id']) {
                          check = true;
                          index = i;
                          break;
                        }
                      }

                      print(UserDetails.regStation[index]);

                      Response response1 =
                          await post(UserDetails.api + 'list_box', body: {
                        'station_id': UserDetails.regStation[index]['_id']
                      });
                      var result1 = jsonDecode(response1.body);
                      print(result1);
                      UserDetails.regBox = result1;

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              RegBox(item: UserDetails.regStation[index]),
                        ),
                      );
                    } else {
                      Fluttertoast.showToast(
                          msg: "error",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 1,
                          backgroundColor: Colors.blue,
                          textColor: Colors.white);
                    }

                    print('scan');
                  },
                  icon: Icon(Icons.settings_overscan),
                )),
            Container(
                padding: EdgeInsets.all(2.0),
                child: IconButton(
                  onPressed: () {
                    print('search');
                    this._searchPressed();
                  },
                  icon: (_searchIcon),
                ))
          ],
        ),
        body: this._searchIcon.icon == Icons.search
            ? _buildListReg()
            : _buildListSearchReg(),
        bottomNavigationBar: Naviagationbar());
  }

  Widget _buildListReg() {
    return ListView.builder(
      itemCount: UserDetails.regStation.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          child: ListStationDisplay(item: UserDetails.regStation[index]),
          onTap: () async {
            Response response = await post(UserDetails.api + 'list_box',
                body: {'station_id': UserDetails.regStation[index]['_id']});
            var result = jsonDecode(response.body);
            UserDetails.regBox = result;

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    RegBox(item: UserDetails.regStation[index]),
              ),
            );
          },
        );
      },
    );
  }

  void _searchRegStation(search_key) async {
    Response response = await post(UserDetails.api + 'list_station_search_key',
        body: {'search_data': search_key});
    var result = jsonDecode(response.body);
    print(result);

    if (this.mounted) {
      setState(() {
        searchDisplay = result;
      });
    }
  }

  Widget _buildListSearchReg() {
    if (!(_searchText.isEmpty)) {
      if (_searchText != previousInput || previousInput == "") {
        searchDisplay = new List();
        previousInput = _searchText;
        var convert = _searchText.toLowerCase();
        this._searchRegStation(convert);
      }
    } else {
      searchDisplay.clear();
      previousInput = "";
    }
    return ListView.builder(
      itemCount: searchDisplay.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          child: ListStationDisplay(item: searchDisplay[index]),
          onTap: () async {
            Response response = await post(UserDetails.api + 'list_box',
                body: {'station_id': searchDisplay[index]['_id']});
            var result = jsonDecode(response.body);
            UserDetails.regBox = result;

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => RegBox(item: searchDisplay[index]),
              ),
            );
          },
        );
      },
    );
  }
}
