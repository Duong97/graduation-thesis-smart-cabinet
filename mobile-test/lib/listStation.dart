import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'scaffold_custom.dart';
import 'listBox.dart';
import 'UserDetail.dart';

class ListStation extends StatefulWidget {
  @override
  _ListStation createState() => _ListStation();
}

class _ListStation extends State<ListStation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          title: new Text("Device"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.blue, Colors.teal[100]],
              ),
            ),
          ),
          actions: <Widget>[
            Container(
                padding: EdgeInsets.all(2.0),
                child: IconButton(
                  onPressed: () async {
                    String qrResult = await UserDetails.Scann_QR();

                    Response response = await post(
                        UserDetails.api + 'query_station_id',
                        body: {'token': qrResult});

                    var result = json.decode(response.body);
                    print(result);

                    if (result['station_id'] != null) {
                      //print(UserDetails.listStation);
                      var index;
                      for (var i = 0; i < UserDetails.listStation.length; i++) {
                        if (UserDetails.listStation[i]['_id'] ==
                            result['station_id']) {
                          index = i;
                          break;
                        }
                      }
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              ListBox(item: UserDetails.listStation[index]),
                        ),
                      );
                    } else {
                      Fluttertoast.showToast(
                          msg: "error",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 1,
                          backgroundColor: Colors.blue,
                          textColor: Colors.white);
                    }

                    print('scan');
                  },
                  icon: Icon(Icons.settings_overscan),
                )),
            Container(
                padding: EdgeInsets.all(2.0),
                child: IconButton(
                  onPressed: () {
                    print('search');
                  },
                  icon: Icon(Icons.search),
                )),
          ],
        ),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                  itemCount: UserDetails.listStation.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: ListStationDisplay(
                          item: UserDetails.listStation[index]),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                ListBox(item: UserDetails.listStation[index]),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ]),
        bottomNavigationBar: Naviagationbar());
  }
}
