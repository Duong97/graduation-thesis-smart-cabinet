import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'RegBox.dart';
import 'login_form.dart';
import 'scaffold_custom.dart';
import 'UserDetail.dart';
import 'package:http/http.dart';
import 'package:geolocator/geolocator.dart';

class Home_screen extends StatefulWidget {
  @override
  _Home_screen createState() => _Home_screen();
}

class _Home_screen extends State<Home_screen> {
  static const platform = const MethodChannel('flutter.native/AndroidPlatform');

  @override
  void initState() {
    super.initState();
    this.testLocation();
    //call Android plaftform
    platform.invokeMethod('startSocket');
    platform.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    if (call.method == "blockUser") {
      var result = jsonDecode(call.arguments);
      if (result['active'] == "false") {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        //Remove String
        prefs.remove("id");
        prefs.remove("email");
        prefs.remove("username");

        final String deleteUserInfo =
            await platform.invokeMethod('deleteUserInfo');

        await post(UserDetails.api + 'logout',
            body: {'user_id': UserDetails.id});

        Fluttertoast.showToast(
            msg: "You has been blocked",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.blue,
            textColor: Colors.white);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LoginScreen()),
            (Route<dynamic> route) => false);

        print('block user');
      }
    }
  }

  void testLocation() async {
    UserDetails.regStation.clear();
    GeolocationStatus geolocationStatus =
        await Geolocator().checkGeolocationPermissionStatus();
    print(geolocationStatus);
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
    if (position != null) {
      Response response =
          await post(UserDetails.api + 'list_station_near_user', body: {
        'latitude': position.latitude.toString(),
        'longitude': position.longitude.toString(),
      });
      var result = jsonDecode(response.body);

      setState(() {
        UserDetails.regStation = result;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var loc = '\u{1F4CD}';
    var map = '\u{1F5FA}';
    var cab_icon = '\u{1F5C4}';

    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.blue, Colors.teal[100]],
              ),
            ),
          ),
          title: Image.asset(
            'image/Logo.png',
            fit: BoxFit.contain,
            height: 45,
          ),
          actions: <Widget>[
            Container(
                padding: EdgeInsets.all(2.0),
                child: IconButton(
                  onPressed: () {
                    print('search');
                  },
                  icon: Icon(Icons.search),
                ))
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(0, 18.0, 0.0, 10.0),
                child: Text(
                  'Near you',
                  style: TextStyle(fontSize: 45.0),
                ),
              ),
              Expanded(
                child: _buildListReg(),
              )
            ],
          ),
        ),
        bottomNavigationBar: Naviagationbar());
  }

  Widget _buildListReg() {
    return ListView.builder(
      itemCount: UserDetails.regStation.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          child: ListStationDisplay(item: UserDetails.regStation[index]),
          onTap: () async {
            Response response = await post(UserDetails.api + 'list_box',
                body: {'station_id': UserDetails.regStation[index]['_id']});
            var result = jsonDecode(response.body);
            UserDetails.regBox = result;

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    RegBox(item: UserDetails.regStation[index]),
              ),
            );
          },
        );
      },
    );
  }
}
