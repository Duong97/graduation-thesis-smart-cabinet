These folders are my contribution to project named Smart Cabinet in 2020. It allows user to open a cabinet by scanning QR code on station's screen.

I would like to say thank you to my great partners, Pham Minh Duy and Do Nguyen Huy Hoang, for cooperating with me in this graduation thesis.

The QR code on station screen will be changed after each 3 or 5 minutes to avoid capture:

<img src="Demo/station_screen.png" width="70%"/>

Opening cabinet demonstration:

![](Demo/open_demo.gif)

The illustration for hiring a cabinet:

Note: This is momo test application for developer so that the money in account is not real. Please visit https://developers.momo.vn/ for more infomation

![](Demo/purchase_demo.gif)

The illustration of authorization feature:

![](Demo/authorize_demo.mp4)

The illustration how blockchain theory is applied in this project:

![](Demo/blockchain_demo.mp4)