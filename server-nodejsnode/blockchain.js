var Order_pending = require("./models/order_pending");
var Order = require("./models/order");
var Order_pendingchain = require("./models/order_pendingchain");

var Payment_pending = require("./models/payment_pending");
var Payment = require("./models/payment");
var Payment_pendingchain = require("./models/payment_pendingchain");

var Open_log_pending = require("./models/open_log_pending");
var Open_log = require("./models/open_log");
var Open_log_pendingchain = require("./models/open_log_pendingchain");

var Authorize_log_pending = require("./models/authorize_log_pending");
var Authorize_log = require("./models/authorize_log");
var Authorize_log_pendingchain = require("./models/authorize_log_pendingchain");

const SHA256 = require("crypto-js/sha256");
const delay = require("delay");

exports.order = async (
  order_id,
  user_id,
  box_id,
  station_id,
  start_time,
  end_time,
  order_time
) => {
  var pendingBlock = {
    order_id: order_id,
    user_id: user_id,
    box_id: box_id,
    station_id: station_id,
    start_time: start_time,
    end_time: end_time,
    order_time: order_time,
    process: 0,
  };

  await Order_pending(pendingBlock).save();
  await this.order_mining();
};

exports.order_mining = async () => {
  var order_mining_state = process.env.ORDER_MINING;

  while (true) {
    if (order_mining_state === "true") break;
    process.env.ORDER_MINING = true;
    if (process.env.ORDER_CHAINS_COMPARISON === "true") {
      await delay(15000);
      continue;
    }

    var order_pending_result = await Order_pending.find({});

    if (order_pending_result.length == 0) {
      console.log("exit mining order");
      process.env.ORDER_MINING = false;
      break;
    } else if (order_pending_result.length > 0) {
      for (var i = 0; i < order_pending_result.length; i++) {
        if (process.env.ORDER_CHAINS_COMPARISON === "true") {
          break;
        }
        currentObject = order_pending_result[i];
        var updateJson = currentObject;
        var newvalues = { process: 1 };

        await Order_pending.updateOne(updateJson, newvalues, function (
          err,
          res
        ) {});

        console.log("start mining order");

        var order_result = await Order.find({});

        var nonce = 0;
        var difficulty = 4;
        var previousHash;

        if (order_result.length == 0) {
          index = 0;
          previousHash = "0";
        } else {
          index = order_result.length;
          previousPosition = order_result.findIndex(
            (element) => element.index == index - 1
          );
          if (previousPosition == -1) {
            break;
          }
          previousHash = order_result[previousPosition].Hash;
        }

        var stop = false;
        while (true) {
          if (process.env.ORDER_REPLACE === "true") {
            console.log("stop mining order");
            stop = true;
            break;
          }
          if (process.env.ORDER_CHAINS_COMPARISON === "true") {
            console.log("New order chain has come");
            await delay(15000);
            continue;
          }

          nonce++;
          var currentHash = SHA256(
            currentObject.order_id +
              currentObject.user_id +
              currentObject.box_id +
              currentObject.station_id +
              currentObject.start_time +
              currentObject.end_time +
              currentObject.order_time +
              nonce +
              previousHash +
              process.env.NODE_NAME
          ).toString();
          if (
            currentHash.substring(0, difficulty) ===
            Array(difficulty + 1).join("0")
          )
            break;
        }
        if (stop == true) {
          i = i + 1;
          process.env.ORDER_REPLACE = false;
          continue;
        }

        var insertBlock = {
          order_id: currentObject.order_id,
          user_id: currentObject.user_id,
          box_id: currentObject.box_id,
          station_id: currentObject.station_id,
          start_time: currentObject.start_time,
          end_time: currentObject.end_time,
          order_time: currentObject.order_time,
          nonce: nonce,
          timestamp: new Date().getTime(),
          previousHash: previousHash,
          Hash: currentHash,
          author: process.env.NODE_NAME,
          index: index,
        };

        await Order(insertBlock).save();
        console.log(`Finish mining order with current hash ${currentHash}`);

        var deleteBlock = {
          order_id: currentObject.order_id,
          user_id: currentObject.user_id,
          box_id: currentObject.box_id,
          station_id: currentObject.station_id,
          start_time: currentObject.start_time,
          end_time: currentObject.end_time,
          order_time: currentObject.order_time,
        };

        await Order_pending.deleteOne(deleteBlock);
        console.log("done");
      }
    }
  }
};

exports.order_broadcast = async (socket) => {
  //listen mongodb
  console.log("order_broadcast");
  await Order_pending.deleteMany({});

  Order_pending.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Order_pending.find({}, async function (err, order_pending_result) {
        if (
          order_pending_result.length == 0 &&
          process.env.ORDER_CHAINS_COMPARISON == "false" &&
          process.env.SEND_TO_MAIN_NODE_ORDER == "false"
        ) {
          process.env.CLEAR_ORDER_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_ORDER = true;

          var order_result = await Order.find({});
          RandExp = require("randexp");
          randexp = new RandExp(/^dhd-[0-9a-zA-Z]{20}/);
          var transfer = {
            chain: order_result,
            from: process.env.NODE_NAME,
            token: randexp.gen(),
          };
          console.log("io new order chain from node");
          socket.emit("new_order_chain_from_node", transfer);
        } else if (
          order_pending_result.length == 0 &&
          process.env.ORDER_CHAINS_COMPARISON == "true" &&
          process.env.SEND_TO_MAIN_NODE_ORDER == "false"
        ) {
          process.env.CLEAR_ORDER_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_ORDER = true;
          console.log("io order chains comparison");
          socket.emit("order_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.order_pendingchain_broadcast = async (socket) => {
  Order_pendingchain.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Order_pendingchain.find({}, async function (
        err,
        order_pendingchain_result
      ) {
        if (
          order_pendingchain_result.length == 0 &&
          process.env.SEND_TO_MAIN_NODE_ORDER == "false" &&
          process.env.CLEAR_ORDER_PENDING == "false"
        ) {
          process.env.SEND_TO_MAIN_NODE_ORDER = true;
          socket.emit("order_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.payment = async (
  pay_service,
  pay_num,
  pay_info,
  pay_date,
  cost,
  order_id
) => {
  var pendingBlock = {
    pay_service: pay_service,
    pay_num: pay_num,
    pay_info: pay_info,
    pay_date: pay_date,
    cost: cost,
    order_id: order_id,
    process: 0,
  };

  await Payment_pending(pendingBlock).save();
  await this.payment_mining();
};

exports.payment_mining = async () => {
  var payment_mining_state = process.env.PAYMENT_MINING;

  while (true) {
    if (payment_mining_state == "true") break;
    process.env.PAYMENT_MINING = true;
    if (process.env.PAYMENT_CHAINS_COMPARISON == "true") {
      await delay(15000);
      continue;
    }

    var payment_pending_result = await Payment_pending.find({});

    if (payment_pending_result.length == 0) {
      console.log("exit mining payment");
      process.env.PAYMENT_MINING = false;
      break;
    } else if (payment_pending_result.length > 0) {
      for (var i = 0; i < payment_pending_result.length; i++) {
        if (process.env.PAYMENT_CHAINS_COMPARISON == "true") {
          break;
        }
        currentObject = payment_pending_result[i];
        var updateJson = currentObject;

        var newvalues = { process: 1 };

        await Payment_pending.updateOne(updateJson, newvalues, function (
          err,
          res
        ) {});

        console.log("start mining payment");

        var payment_result = await Payment.find({});
        var nonce = 0;
        var difficulty = 4;
        var previousHash;

        if (payment_result.length == 0) {
          index = 0;
          previousHash = "0";
        } else {
          index = payment_result.length;
          previousPosition = payment_result.findIndex(
            (element) => element.index == index - 1
          );
          if (previousPosition == -1) {
            break;
          }
          previousHash = payment_result[previousPosition].Hash;
        }

        var stop = false;
        while (true) {
          if (process.env.PAYMENT_REPLACE == "true") {
            console.log("stop mining payment");
            stop = true;
            break;
          }
          if (process.env.PAYMENT_CHAINS_COMPARISON == "true") {
            console.log("New payment chain has come");
            await delay(15000);

            continue;
          }

          nonce++;
          var currentHash = SHA256(
            currentObject.pay_service +
              currentObject.pay_num +
              JSON.stringify(currentObject.pay_info) +
              currentObject.pay_date +
              currentObject.cost +
              currentObject.order_id +
              nonce +
              previousHash +
              process.env.NODE_NAME
          ).toString();

          if (
            currentHash.substring(0, difficulty) ===
            Array(difficulty + 1).join("0")
          )
            break;
        }
        if (stop == true) {
          i = i + 1;
          process.env.PAYMENT_REPLACE = false;
          continue;
        }
        var insertBlock = {
          pay_service: currentObject.pay_service,
          pay_num: currentObject.pay_num,
          pay_info: currentObject.pay_info,
          pay_date: currentObject.pay_date,
          cost: currentObject.cost,
          order_id: currentObject.order_id,
          nonce: nonce,
          timestamp: new Date().getTime(),
          previousHash: previousHash,
          Hash: currentHash,
          author: process.env.NODE_NAME,
          index: index,
        };

        await Payment(insertBlock).save();
        console.log(`Finish mining payment with current hash ${currentHash}`);

        var deleteBlock = {
          pay_service: currentObject.pay_service,
          pay_num: currentObject.pay_num,
          pay_info: currentObject.pay_info,
          pay_date: currentObject.pay_date,
          cost: currentObject.cost,
          order_id: currentObject.order_id,
        };

        await Payment_pending.deleteOne(deleteBlock);
        console.log("done");
      }
    }
  }
};

exports.payment_broadcast = async (socket) => {
  //listen mongodb
  console.log("payment_broadcast");

  await Payment_pending.deleteMany({});

  Payment_pending.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Payment_pending.find({}, async function (err, payment_pending_result) {
        if (
          payment_pending_result.length == 0 &&
          process.env.PAYMENT_CHAINS_COMPARISON == "false" &&
          process.env.SEND_TO_MAIN_NODE_PAYMENT == "false"
        ) {
          process.env.CLEAR_PAYMENT_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_PAYMENT = true;
          var payment_result = await Payment.find({});
          RandExp = require("randexp");
          randexp = new RandExp(/^dhd-[0-9a-zA-Z]{20}/);
          var transfer = {
            chain: payment_result,
            from: process.env.NODE_NAME,
            token: randexp.gen(),
          };
          console.log("io new payment chain from node");
          socket.emit("new_payment_chain_from_node", transfer);
        } else if (
          payment_pending_result.length == 0 &&
          process.env.PAYMENT_CHAINS_COMPARISON == "true" &&
          process.env.SEND_TO_MAIN_NODE_PAYMENT == "false"
        ) {
          process.env.CLEAR_PAYMENT_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_PAYMENT = true;
          console.log("io payment chains comparison");
          socket.emit("payment_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.payment_pendingchain_broadcast = async (socket) => {
  Payment_pendingchain.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Payment_pendingchain.find({}, async function (
        err,
        payment_pendingchain_result
      ) {
        if (
          payment_pendingchain_result.length == 0 &&
          process.env.SEND_TO_MAIN_NODE_PAYMENT == "false" &&
          process.env.CLEAR_PAYMENT_PENDING == "false"
        ) {
          process.env.SEND_TO_MAIN_NODE_PAYMENT = true;
          socket.emit("payment_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.open_log = async (user_id, box_id, station_id, open_time) => {
  var pendingBlock = {
    user_id: user_id,
    box_id: box_id,
    station_id: station_id,
    open_time: open_time,
    process: 0,
  };

  await Open_log_pending(pendingBlock).save();
  await this.open_log_mining();
};

exports.open_log_mining = async () => {
  var open_log_mining_state = process.env.OPEN_LOG_MINING;

  while (true) {
    if (open_log_mining_state == "true") break;
    process.env.OPEN_LOG_MINING = true;
    if (process.env.OPEN_LOG_CHAINS_COMPARISON == "true") {
      await delay(15000);
      continue;
    }

    var open_log_pending_result = await Open_log_pending.find({});

    if (open_log_pending_result.length == 0) {
      console.log("exit mining open log");
      process.env.OPEN_LOG_MINING = false;
      break;
    } else if (open_log_pending_result.length > 0) {
      for (var i = 0; i < open_log_pending_result.length; i++) {
        if (process.env.OPEN_LOG_CHAINS_COMPARISON === "true") {
          break;
        }
        currentObject = open_log_pending_result[i];
        var updateJson = currentObject;
        var newvalues = { process: 1 };

        await Open_log_pending.updateOne(updateJson, newvalues, function (
          err,
          res
        ) {});

        console.log("start mining open log");

        var open_log_result = await Open_log.find({});

        var nonce = 0;
        var difficulty = 4;
        var previousHash;

        if (open_log_result.length == 0) {
          index = 0;
          previousHash = "0";
        } else {
          index = open_log_result.length;
          previousPosition = open_log_result.findIndex(
            (element) => element.index == index - 1
          );
          if (previousPosition == -1) {
            break;
          }
          previousHash = open_log_result[previousPosition].Hash;
        }
        var stop = false;
        while (true) {
          if (process.env.OPEN_LOG_REPLACE === "true") {
            console.log("stop mining open log");
            stop = true;
            break;
          }
          if (process.env.OPEN_LOG_CHAINS_COMPARISON === "true") {
            console.log("New open log chain has come");
            await delay(15000);
            continue;
          }

          nonce++;
          var currentHash = SHA256(
            currentObject.user_id +
              currentObject.box_id +
              currentObject.station_id +
              currentObject.open_time +
              nonce +
              previousHash +
              process.env.NODE_NAME
          ).toString();
          if (
            currentHash.substring(0, difficulty) ===
            Array(difficulty + 1).join("0")
          )
            break;
        }
        if (stop == true) {
          i = i + 1;
          process.env.OPEN_LOG_REPLACE = false;
          continue;
        }

        var insertBlock = {
          user_id: currentObject.user_id,
          box_id: currentObject.box_id,
          station_id: currentObject.station_id,
          open_time: currentObject.open_time,
          nonce: nonce,
          timestamp: new Date().getTime(),
          previousHash: previousHash,
          Hash: currentHash,
          author: process.env.NODE_NAME,
          index: index,
        };

        await Open_log(insertBlock).save();
        console.log(`Finish mining open log with current hash ${currentHash}`);

        var deleteBlock = {
          user_id: currentObject.user_id,
          box_id: currentObject.box_id,
          station_id: currentObject.station_id,
          open_time: currentObject.open_time,
        };

        await Open_log_pending.deleteOne(deleteBlock);
        console.log("done");
      }
    }
  }
};

exports.open_log_broadcast = async (socket) => {
  //listen mongodb
  console.log("open_log_broadcast");
  await Open_log_pending.deleteMany({});
  Open_log_pending.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Open_log_pending.find({}, async function (err, open_log_pending_result) {
        if (
          open_log_pending_result.length == 0 &&
          process.env.OPEN_LOG_CHAINS_COMPARISON == "false" &&
          process.env.SEND_TO_MAIN_NODE_OPEN_LOG == "false"
        ) {
          process.env.CLEAR_OPEN_LOG_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_OPEN_LOG = true;
          var open_log_result = await Open_log.find({});
          RandExp = require("randexp");
          randexp = new RandExp(/^dhd-[0-9a-zA-Z]{20}/);
          var transfer = {
            chain: open_log_result,
            from: process.env.NODE_NAME,
            token: randexp.gen(),
          };
          console.log("io new open log chain from node");
          socket.emit("new_open_log_chain_from_node", transfer);
        } else if (
          open_log_pending_result.length == 0 &&
          process.env.OPEN_LOG_CHAINS_COMPARISON == "true" &&
          process.env.SEND_TO_MAIN_NODE_OPEN_LOG == "false"
        ) {
          process.env.CLEAR_OPEN_LOG_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_OPEN_LOG = true;
          console.log("io open log chains comparison");
          socket.emit("open_log_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.open_log_pendingchain_broadcast = async (socket) => {
  Open_log_pendingchain.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Open_log_pendingchain.find({}, async function (
        err,
        open_log_pendingchain_result
      ) {
        if (
          open_log_pendingchain_result.length == 0 &&
          process.env.SEND_TO_MAIN_NODE_OPEN_LOG == "false" &&
          process.env.CLEAR_OPEN_LOG_PENDING == "false"
        ) {
          process.env.SEND_TO_MAIN_NODE_OPEN_LOG = true;
          socket.emit("open_log_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.authorize_log = async (
  owner_id,
  authorize_id,
  box_id,
  station_id,
  limit,
  action,
  authorize_time
) => {
  var pendingBlock = {
    owner_id: owner_id,
    authorize_id: authorize_id,
    box_id: box_id,
    station_id: station_id,
    limit: limit,
    action: action,
    authorize_time,
    process: 0,
  };

  await Authorize_log_pending(pendingBlock).save();
  await this.authorize_log_mining();
};

exports.authorize_log_mining = async () => {
  var authorize_log_mining_state = process.env.AUTHORIZE_LOG_MINING;
  await delay(10000);

  while (true) {
    if (authorize_log_mining_state === "true") break;
    process.env.AUTHORIZE_LOG_MINING = true;
    if (process.env.AUTHORIZE_LOG_CHAINS_COMPARISON === "true") {
      await delay(15000);
      continue;
    }

    var authorize_log_pending_result = await Authorize_log_pending.find({});

    if (authorize_log_pending_result.length == 0) {
      console.log("exit mining authorize log");
      process.env.AUTHORIZE_LOG_MINING = false;
      break;
    } else if (authorize_log_pending_result.length > 0) {
      for (var i = 0; i < authorize_log_pending_result.length; i++) {
        if (process.env.AUTHORIZE_LOG_CHAINS_COMPARISON === "true") {
          break;
        }
        currentObject = authorize_log_pending_result[i];
        var updateJson = currentObject;

        var newvalues = { process: 1 };

        await Authorize_log_pending.updateOne(updateJson, newvalues, function (
          err,
          res
        ) {});

        console.log("start mining authorize log");

        var authorize_log_result = await Authorize_log.find({});

        var nonce = 0;
        var difficulty = 4;
        var previousHash;

        if (authorize_log_result.length == 0) {
          index = 0;
          previousHash = "0";
        } else {
          index = authorize_log_result.length;
          previousPosition = authorize_log_result.findIndex(
            (element) => element.index == index - 1
          );
          if (previousPosition == -1) {
            break;
          }
          previousHash = authorize_log_result[previousPosition].Hash;
        }

        var stop = false;
        while (true) {
          if (process.env.AUTHORIZE_LOG_REPLACE === "true") {
            console.log("stop mining authorize log");
            stop = true;
            break;
          }
          if (process.env.AUTHORIZE_LOG_CHAINS_COMPARISON === "true") {
            console.log("New authorize log chain has come");
            await delay(15000);
            continue;
          }
          nonce++;

          var currentHash = SHA256(
            currentObject.owner_id +
              currentObject.authorize_id +
              currentObject.box_id +
              currentObject.station_id +
              currentObject.limit +
              currentObject.action +
              currentObject.authorize_time +
              nonce +
              previousHash +
              process.env.NODE_NAME
          ).toString();

          if (
            currentHash.substring(0, difficulty) ===
            Array(difficulty + 1).join("0")
          )
            break;
        }
        if (stop == true) {
          i = i + 1;
          process.env.AUTHORIZE_LOG_REPLACE = false;
          continue;
        }

        var insertBlock = {
          authorize_id: currentObject.authorize_id,
          owner_id: currentObject.owner_id,
          box_id: currentObject.box_id,
          station_id: currentObject.station_id,
          limit: currentObject.limit,
          action: currentObject.action,
          authorize_time: currentObject.authorize_time,
          nonce: nonce,
          timestamp: new Date().getTime(),
          previousHash: previousHash,
          Hash: currentHash,
          author: process.env.NODE_NAME,
          index: index,
        };

        await Authorize_log(insertBlock).save();
        console.log(
          `Finish mining authorize log with current hash ${currentHash}`
        );

        var deleteBlock = {
          owner_id: currentObject.owner_id,
          authorize_id: currentObject.authorize_id,
          box_id: currentObject.box_id,
          station_id: currentObject.station_id,
          limit: currentObject.limit,
          action: currentObject.action,
          authorize_time: currentObject.authorize_time,
        };

        await Authorize_log_pending.deleteOne(deleteBlock);
        console.log("done");
      }
    }
  }
};

exports.authorize_log_broadcast = async (socket) => {
  //listen mongodb
  console.log("authorize_log_broadcast");

  await Authorize_log_pending.deleteMany({});

  Authorize_log_pending.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Authorize_log_pending.find({}, async function (
        err,
        authorize_log_pending_result
      ) {
        if (
          authorize_log_pending_result.length == 0 &&
          process.env.AUTHORIZE_LOG_CHAINS_COMPARISON == "false" &&
          process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG == "false"
        ) {
          process.env.CLEAR_AUTHORIZE_LOG_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG = true;
          var authorize_log_result = await Authorize_log.find({});
          RandExp = require("randexp");
          randexp = new RandExp(/^dhd-[0-9a-zA-Z]{20}/);

          var transfer = {
            chain: authorize_log_result,
            from: process.env.NODE_NAME,
            token: randexp.gen(),
          };
          console.log("io new authorize log chain from node");
          socket.emit("new_authorize_log_chain_from_node", transfer);
        } else if (
          authorize_log_pending_result.length == 0 &&
          process.env.AUTHORIZE_LOG_CHAINS_COMPARISON == "true" &&
          process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG == "false"
        ) {
          process.env.CLEAR_AUTHORIZE_LOG_PENDING = true;
          process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG = true;
          console.log("io authorize log chains comparison");
          socket.emit("authorize_log_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};

exports.authorize_log_pendingchain_broadcast = async (socket) => {
  Authorize_log_pendingchain.watch().on("change", async function (result) {
    if (result.operationType == "delete") {
      Authorize_log_pendingchain.find({}, async function (
        err,
        authorize_log_pendingchain_result
      ) {
        if (
          authorize_log_pendingchain_result.length == 0 &&
          process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG == "false" &&
          process.env.CLEAR_AUTHORIZE_LOG_PENDING == "false"
        ) {
          process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG = true;
          socket.emit("authorize_log_chains_comparison", {
            from: process.env.NODE_NAME,
            status: "finish",
          });
        }
      });
    }
  });
};
