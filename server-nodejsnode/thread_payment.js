const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_payment");
console.log("New payment");
console.log(workerData);
mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.payment(
      workerData.pay_service,
      workerData.pay_num,
      workerData.pay_info,
      workerData.pay_date,
      workerData.cost,
      workerData.order_id
    );
    mongoose.disconnect();
  }
);
