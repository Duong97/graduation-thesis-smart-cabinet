var deepEqual = require("deep-equal");
var mongoose = require("mongoose");

const { workerData, parentPort } = require("worker_threads");

var Payment = require("./models/payment");

var Payment_pending = require("./models/payment_pending");

mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    var validChain = workerData.validChain;
    var insideDatabaseChain = workerData.insideDatabaseChain;

    console.log(
      `separate valid payment chain with validChain length ${validChain.length} and payment insideDatabaseChain ${insideDatabaseChain.length}`
    );

    //solution in tester;
    for (var i = 0; i < validChain.length; i++) {
      var currentObject = Object.assign({}, validChain[i]);
      if (i < insideDatabaseChain.length) {
        var insideDatabaseObject = Object.assign({}, insideDatabaseChain[i]);
        delete currentObject.__v;
        delete currentObject._id;
        delete insideDatabaseObject.__v;
        delete insideDatabaseObject._id;
        if (!deepEqual(currentObject, insideDatabaseObject)) {
          await Payment.updateOne(
            { _id: insideDatabaseChain[i]._id },
            currentObject,
            function (err, res) {}
          );
        }
      } else {
        var insertBlock = Object.assign({}, validChain[i]);
        delete insertBlock._id;
        await Payment(insertBlock).save();
        var deleteBlock = Object.assign({}, validChain[i]);
        delete deleteBlock.__v;
        delete deleteBlock.index;
        delete deleteBlock.author;
        delete deleteBlock.Hash;
        delete deleteBlock.previousHash;
        delete deleteBlock.timestamp;
        delete deleteBlock.nonce;
        delete deleteBlock._id;
        await Payment_pending.deleteOne(deleteBlock);
      }
    }

    mongoose.disconnect();
  }
);
