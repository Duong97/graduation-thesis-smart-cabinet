const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const OpenLogSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  open_time: {
    type: String,
    required: true,
  },

  nonce: {
    type: Number,
    required: true,
  },

  timestamp: {
    type: Number,
    required: true,
  },

  previousHash: {
    type: String,
    required: true,
  },

  Hash: {
    type: String,
    required: true,
  },

  author: {
    type: String,
    required: true,
    default: "unknown",
  },

  index: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model("open_log", OpenLogSchema, "open_log");
