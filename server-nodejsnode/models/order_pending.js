const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const OrderPendingSchema = new Schema({
  order_id: {
    type: String,
    required: true,
  },
  user_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  start_time: {
    type: String,
    required: true,
  },

  end_time: {
    type: String,
    required: true,
  },

  order_time: {
    type: String,
    required: true,
  },

  process: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "order_pending",
  OrderPendingSchema,
  "order_pending"
);
