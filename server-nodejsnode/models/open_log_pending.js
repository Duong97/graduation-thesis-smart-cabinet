const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const OpenLogPendingSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  open_time: {
    type: String,
    required: true,
  },

  process: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "open_log_pending",
  OpenLogPendingSchema,
  "open_log_pending"
);
