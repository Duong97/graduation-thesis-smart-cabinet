const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const AuthLogSchema = new Schema({
  authorize_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  owner_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  limit: {
    type: String,
    required: true,
  },

  action: {
    type: String,
    required: true,
  },

  authorize_time: {
    type: String,
    required: true,
  },

  nonce: {
    type: Number,
    required: true,
  },

  timestamp: {
    type: Number,
    required: true,
  },

  previousHash: {
    type: String,
    required: true,
  },

  Hash: {
    type: String,
    required: true,
  },

  author: {
    type: String,
    required: true,
  },

  index: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "authorize_log",
  AuthLogSchema,
  "authorize_log"
);
