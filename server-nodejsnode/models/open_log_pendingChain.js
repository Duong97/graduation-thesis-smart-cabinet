const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const OpenLogPendingChainSchema = new Schema({
  chain: {
    type: Array,

    required: true,
  },

  from: {
    type: String,

    required: true,
  },

  token: {
    type: String,

    required: true,
  },
  index: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "open_log_pendingchain",
  OpenLogPendingChainSchema,
  "open_log_pendingchain"
);
