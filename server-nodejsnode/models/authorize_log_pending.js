const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const AuthLogPendingSchema = new Schema({
  authorize_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  owner_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,

    required: true,
  },

  limit: {
    type: String,
    required: true,
  },

  action: {
    type: String,
    required: true,
  },

  authorize_time: {
    type: String,
    required: true,
  },

  process: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "authorize_log_pending",
  AuthLogPendingSchema,
  "authorize_log_pending"
);
