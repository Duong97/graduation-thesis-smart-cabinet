const { Worker } = require("worker_threads");

var mongoose = require("mongoose");
const SHA256 = require("crypto-js/sha256");
var auxiliary = require("./auxiliary");
const delay = require("delay");

var Open_log = require("./models/open_log");
var Open_log_pendingchain = require("./models/open_log_pendingchain");

console.log("thread_update_openlogChain");

mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    //receive comparison chain

    while (true) {
      await delay(1000);
      var open_log_pendingchain_result = await Open_log_pendingchain.find({});
      open_log_pendingchain_result = JSON.parse(
        JSON.stringify(open_log_pendingchain_result)
      );

      if (open_log_pendingchain_result.length == 0) {
        await delay(10000);
        process.env.OPEN_LOG_REPLACE = false;
        process.env.OPEN_LOG_CHAINS_COMPARISON = false;
        console.log("finish open log chain comparison procedure");
        break;
      } else {
        var list_token = [];
        for (var i = 0; i < open_log_pendingchain_result.length; i++) {
          var found = list_token.findIndex(
            (elementName) =>
              elementName == open_log_pendingchain_result[i].token
          );
          if (found == -1)
            list_token.push(open_log_pendingchain_result[i].token);
        }

        //start
        console.log(list_token);
        var newChains = [];

        for (var i = 0; i < list_token.length; i++) {
          var targetChain = await auxiliary.rebuild_openLog(
            list_token[i],
            open_log_pendingchain_result
          );
          newChains.push(targetChain);
        }

        //detect valid chain
        var trigger = false;
        var open_log_result = await Open_log.find({});
        open_log_result = JSON.parse(JSON.stringify(open_log_result));
        upDateChain = open_log_result;
        for (var i = 0; i < newChains.length; i++) {
          if (
            newChains[i].length > upDateChain.length &&
            chainValidation(newChains[i])
          ) {
            console.log("detect new available open log chain");
            upDateChain = newChains[i];
            trigger = true;
          } else if (
            newChains[i].length == upDateChain.length &&
            chainValidation(newChains[i])
          ) {
            object1 = newChains[i];
            object2 = upDateChain;
            if (chainValidation(upDateChain)) {
              temp1 = object1[object1.length - 1].timestamp;
              temp2 = object2[object2.length - 1].timestamp;
              if (temp1 < temp2) {
                console.log("detect new available order chain");
                upDateChain = newChains[i];
                trigger = true;
              }
            } else {
              console.log("detect new available order chain");
              upDateChain = newChains[i];
              trigger = true;
            }
          }
        }

        //update current chain If a new valid chain exists
        if (trigger == true) {
          console.log("update_open_log_chain");
          process.env.OPEN_LOG_REPLACE = true;

          var segmentLength = 200;
          var initLength_upDateChain = upDateChain.length;
          var numberThread = 0;
          var completeThread = 0;

          while (true) {
            await delay(1000);
            numberThread += 1;
            if (initLength_upDateChain > segmentLength) {
              initLength_upDateChain -= segmentLength;
              var cutupDateChain = await upDateChain.slice(0, segmentLength);
              upDateChain = await upDateChain.slice(segmentLength);
              var cutopen_log_result = await open_log_result.slice(
                0,
                segmentLength
              );
              open_log_result = await open_log_result.slice(segmentLength);

              var thread_update_openlogChain1_data = {
                validChain: cutupDateChain,
                insideDatabaseChain: cutopen_log_result,
              };

              const worker = new Worker("./thread_update_openlogChain1.js", {
                workerData: thread_update_openlogChain1_data,
              });

              worker.on("exit", (code) => {
                completeThread += 1;
                console.log(`open log segment ${completeThread} is completed`);
              });
            } else {
              var thread_update_openlogChain1_data = {
                validChain: upDateChain,
                insideDatabaseChain: open_log_result,
              };

              var worker = new Worker("./thread_update_openlogChain1.js", {
                workerData: thread_update_openlogChain1_data,
              });

              worker.on("exit", (code) => {
                completeThread += 1;
                console.log(`open log segment ${completeThread} is completed`);
              });
              break;
            }
          }

          while (true) {
            console.log("update open log");
            if (numberThread == completeThread) break;
            await delay(10000);
          }
        }

        //delete array in open_log_pendingchain
        for (var i = 0; i < list_token.length; i++) {
          await Open_log_pendingchain.deleteMany(
            { token: list_token[i] },
            function (err, res) {}
          );
        }
      }
    }

    mongoose.disconnect();
  }
);

function chainValidation(chain) {
  var check = true;

  for (var i = 0; i < chain.length; i++) {
    if (chain[i].index == 0) {
      if (
        !(
          chain[i].previousHash == "0" &&
          chain[i].Hash ==
            SHA256(
              chain[i].user_id +
                chain[i].box_id +
                chain[i].station_id +
                chain[i].open_time +
                chain[i].nonce +
                chain[i].previousHash +
                chain[i].author
            ).toString()
        )
      ) {
        check = false;
        break;
      }
    } else {
      previousPosition = chain.findIndex(
        (element) => element.index == chain[i].index - 1
      );
      if (previousPosition == -1) {
        check = false;
        break;
      }
      previousBlock = chain[previousPosition];
      currentBlock = chain[i];
      if (
        !(
          currentBlock.previousHash == previousBlock.Hash &&
          currentBlock.Hash ==
            SHA256(
              chain[i].user_id +
                chain[i].box_id +
                chain[i].station_id +
                chain[i].open_time +
                chain[i].nonce +
                chain[i].previousHash +
                chain[i].author
            ).toString()
        )
      ) {
        check = false;
        break;
      }
    }
  }

  console.log(`validate open log chain: ${check}`);
  if (check) return true;
  else return false;
}
