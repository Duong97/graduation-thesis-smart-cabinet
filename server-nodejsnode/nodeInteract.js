const { Worker } = require("worker_threads");

var Order_pendingchain = require("./models/order_pendingchain");
process.env.SEND_TO_MAIN_NODE_ORDER = false;
process.env.CLEAR_ORDER_PENDING = false;

var Payment_pendingchain = require("./models/payment_pendingchain");
process.env.SEND_TO_MAIN_NODE_PAYMENT = false;
process.env.CLEAR_PAYMENT_PENDING = false;

var Open_log_pendingchain = require("./models/open_log_pendingchain");
process.env.SEND_TO_MAIN_NODE_OPEN_LOG = false;
process.env.CLEAR_OPEN_LOG_PENDING = false;

var Authorize_log_pendingchain = require("./models/authorize_log_pendingchain");
process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG = false;
process.env.CLEAR_AUTHORIZE_LOG_PENDING = false;

//for order table
process.env.ORDER_BROADCAST = true;
process.env.ORDER_MINING = false;
process.env.ORDER_CHAINS_COMPARISON = false;
process.env.ORDER_REPLACE = false;

//for payment table
process.env.PAYMENT_BROADCAST = true;
process.env.PAYMENT_MINING = false;
process.env.PAYMENT_CHAINS_COMPARISON = false;
process.env.PAYMENT_REPLACE = false;

//for open_log table
process.env.OPEN_LOG_BROADCAST = true;
process.env.OPEN_LOG_MINING = false;
process.env.OPEN_LOG_CHAINS_COMPARISON = false;
process.env.OPEN_LOG_REPLACE = false;

//for authorize_log table
process.env.AUTHORIZE_LOG_BROADCAST = true;
process.env.AUTHORIZE_LOG_MINING = false;
process.env.AUTHORIZE_LOG_CHAINS_COMPARISON = false;
process.env.AUTHORIZE_LOG_REPLACE = false;

exports.Internal = (socket) => {
  socket.on("socket_id", function (data) {
    socket_id = data;
    console.log(data);
    socket.emit("Node", {
      socket_id: socket_id,
      name: process.env.NODE_NAME,
    });
  });

  //order_broadcast
  socket.on("new_order", function (data) {
    process.env.CLEAR_ORDER_PENDING = false;
    process.env.SEND_TO_MAIN_NODE_ORDER = false;
    var workerData = data;
    new Worker("./thread_order.js", { workerData });
  });

  socket.on("new_order_chain_from_main", async function (data) {
    if (data.from != process.env.NODE_NAME) {
      console.log("new_order_chain_from_main");
      var chain = data.chain;
      console.log(`last element from ${data.from}`);
      console.log(chain[chain.length - 1]);

      segmentLength = 500;
      initLength = chain.length;
      index = 0;

      while (true) {
        if (initLength > segmentLength) {
          initLength -= segmentLength;
          var cutChain = await chain.slice(0, segmentLength);
          chain = await chain.slice(segmentLength);

          var transfer = {
            chain: cutChain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Order_pendingchain(transfer).save();
          index += 1;
        } else {
          var transfer = {
            chain: chain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Order_pendingchain(transfer).save();
          index += 1;
          break;
        }
      }

      if (process.env.ORDER_CHAINS_COMPARISON === "false") {
        process.env.ORDER_CHAINS_COMPARISON = true;
        process.env.ORDER_REPLACE = false;

        new Worker("./thread_update_orderChain.js", {});
      }
    }
  });

  //payment_broadcast
  socket.on("new_payment", function (data) {
    process.env.CLEAR_PAYMENT_PENDING = false;
    process.env.SEND_TO_MAIN_NODE_PAYMENT = false;
    var workerData = data;
    const worker = new Worker("./thread_payment.js", { workerData });
  });

  socket.on("new_payment_chain_from_main", async function (data) {
    if (data.from != process.env.NODE_NAME) {
      console.log("new_payment_chain_from_main");
      var chain = data.chain;
      console.log(`last element from ${data.from}`);
      console.log(chain[chain.length - 1]);

      segmentLength = 500;
      initLength = chain.length;
      index = 0;

      while (true) {
        if (initLength > segmentLength) {
          initLength -= segmentLength;
          var cutChain = await chain.slice(0, segmentLength);
          chain = await chain.slice(segmentLength);

          var transfer = {
            chain: cutChain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Payment_pendingchain(transfer).save();
          index += 1;
        } else {
          var transfer = {
            chain: chain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Payment_pendingchain(transfer).save();
          index += 1;
          break;
        }
      }

      if (process.env.PAYMENT_CHAINS_COMPARISON === "false") {
        process.env.PAYMENT_CHAINS_COMPARISON = true;
        process.env.PAYMENT_REPLACE = false;

        new Worker("./thread_update_paymentChain.js", {});
      }
    }
  });

  //open_log_broadcast
  socket.on("new_open_log", function (data) {
    process.env.CLEAR_OPEN_LOG_PENDING = false;
    process.env.SEND_TO_MAIN_NODE_OPEN_LOG = false;
    var workerData = data;
    const worker = new Worker("./thread_open_log.js", { workerData });
  });

  socket.on("new_open_log_chain_from_main", async function (data) {
    if (data.from != process.env.NODE_NAME) {
      console.log("new_open_log_chain_from_main");
      var chain = data.chain;
      console.log(`last element from ${data.from}`);
      console.log(chain[chain.length - 1]);

      segmentLength = 500;
      initLength = chain.length;
      index = 0;

      while (true) {
        if (initLength > segmentLength) {
          initLength -= segmentLength;
          var cutChain = await chain.slice(0, segmentLength);
          chain = await chain.slice(segmentLength);

          var transfer = {
            chain: cutChain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Open_log_pendingchain(transfer).save();
          index += 1;
        } else {
          var transfer = {
            chain: chain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Open_log_pendingchain(transfer).save();
          index += 1;
          break;
        }
      }

      if (process.env.OPEN_LOG_CHAINS_COMPARISON === "false") {
        process.env.OPEN_LOG_CHAINS_COMPARISON = true;
        process.env.OPEN_LOG_REPLACE = false;
        const worker = new Worker("./thread_update_openlogChain.js", {});
      }
    }
  });

  //authorize_log_broadcast
  socket.on("new_authorize_log", function (data) {
    process.env.CLEAR_AUTHORIZE_LOG_PENDING = false;
    process.env.SEND_TO_MAIN_NODE_AUTHORIZE_LOG = false;
    var workerData = data;
    new Worker("./thread_authorize_log.js", { workerData });
  });

  socket.on("new_authorize_log_chain_from_main", async function (data) {
    if (data.from != process.env.NODE_NAME) {
      console.log("new_authorize_log_chain_from_main");
      var chain = data.chain;
      console.log(`last element from ${data.from}`);
      console.log(chain[chain.length - 1]);

      segmentLength = 500;
      initLength = chain.length;
      index = 0;

      while (true) {
        if (initLength > segmentLength) {
          initLength -= segmentLength;
          var cutChain = await chain.slice(0, segmentLength);
          chain = await chain.slice(segmentLength);

          var transfer = {
            chain: cutChain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Authorize_log_pendingchain(transfer).save();
          index += 1;
        } else {
          var transfer = {
            chain: chain,
            from: data.from,
            token: data.token,
            index: index,
          };

          await Authorize_log_pendingchain(transfer).save();
          index += 1;
          break;
        }
      }

      if (process.env.AUTHORIZE_LOG_CHAINS_COMPARISON === "false") {
        process.env.AUTHORIZE_LOG_CHAINS_COMPARISON = true;
        process.env.AUTHORIZE_LOG_REPLACE = false;

        new Worker("./thread_update_authorizelogChain.js", {});
      }
    }
  });
};
