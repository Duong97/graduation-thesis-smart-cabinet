exports.rebuild_order = async (token, rawOrderChain) => {
  var order_chain_unit = [];
  var connect_order_chain_unit = [];
  for (var j = 0; j < rawOrderChain.length; j++) {
    if (token == rawOrderChain[j].token) {
      order_chain_unit.push(rawOrderChain[j]);
    }
  }

  for (var i = 0; i < order_chain_unit.length; i++) {
    for (var j = i + 1; j < order_chain_unit.length; j++) {
      if (order_chain_unit[i].index > order_chain_unit[j].index) {
        var temp = order_chain_unit[i];
        order_chain_unit[i] = order_chain_unit[j];
        order_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < order_chain_unit.length; k++) {
    connect_order_chain_unit = connect_order_chain_unit.concat(
      order_chain_unit[k].chain
    );
  }
  return connect_order_chain_unit;
};

exports.rebuild_payment = async (token, rawPaymentChain) => {
  var payment_chain_unit = [];
  var connect_payment_chain_unit = [];
  for (var j = 0; j < rawPaymentChain.length; j++) {
    if (token == rawPaymentChain[j].token) {
      payment_chain_unit.push(rawPaymentChain[j]);
    }
  }

  for (var i = 0; i < payment_chain_unit.length; i++) {
    for (var j = i + 1; j < payment_chain_unit.length; j++) {
      if (payment_chain_unit[i].index > payment_chain_unit[j].index) {
        var temp = payment_chain_unit[i];
        payment_chain_unit[i] = payment_chain_unit[j];
        payment_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < payment_chain_unit.length; k++) {
    connect_payment_chain_unit = connect_payment_chain_unit.concat(
      payment_chain_unit[k].chain
    );
  }
  return connect_payment_chain_unit;
};

exports.rebuild_openLog = async (token, rawOpenLogChain) => {
  var open_log_chain_unit = [];
  var connect_open_log_chain_unit = [];
  for (var j = 0; j < rawOpenLogChain.length; j++) {
    if (token == rawOpenLogChain[j].token) {
      open_log_chain_unit.push(rawOpenLogChain[j]);
    }
  }

  for (var i = 0; i < open_log_chain_unit.length; i++) {
    for (var j = i + 1; j < open_log_chain_unit.length; j++) {
      if (open_log_chain_unit[i].index > open_log_chain_unit[j].index) {
        var temp = open_log_chain_unit[i];
        open_log_chain_unit[i] = open_log_chain_unit[j];
        open_log_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < open_log_chain_unit.length; k++) {
    connect_open_log_chain_unit = connect_open_log_chain_unit.concat(
      open_log_chain_unit[k].chain
    );
  }
  return connect_open_log_chain_unit;
};

exports.rebuild_authorizeLog = async (token, rawAuthorizeLogChain) => {
  var authorize_log_chain_unit = [];
  var connect_authorize_log_chain_unit = [];
  for (var j = 0; j < rawAuthorizeLogChain.length; j++) {
    if (token == rawAuthorizeLogChain[j].token) {
      authorize_log_chain_unit.push(rawAuthorizeLogChain[j]);
    }
  }

  for (var i = 0; i < authorize_log_chain_unit.length; i++) {
    for (var j = i + 1; j < authorize_log_chain_unit.length; j++) {
      if (
        authorize_log_chain_unit[i].index > authorize_log_chain_unit[j].index
      ) {
        var temp = authorize_log_chain_unit[i];
        authorize_log_chain_unit[i] = authorize_log_chain_unit[j];
        authorize_log_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < authorize_log_chain_unit.length; k++) {
    connect_authorize_log_chain_unit = connect_authorize_log_chain_unit.concat(
      authorize_log_chain_unit[k].chain
    );
  }
  return connect_authorize_log_chain_unit;
};
