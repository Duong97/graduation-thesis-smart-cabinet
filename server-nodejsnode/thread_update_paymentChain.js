var mongoose = require("mongoose");
const SHA256 = require("crypto-js/sha256");
var auxiliary = require("./auxiliary");
const delay = require("delay");
const { Worker } = require("worker_threads");

var Payment = require("./models/payment");
var Payment_pendingchain = require("./models/payment_pendingchain");

console.log("thread_update_paymentChain");
mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    while (true) {
      await delay(1000);
      var payment_pendingchain_result = await Payment_pendingchain.find({});
      payment_pendingchain_result = JSON.parse(
        JSON.stringify(payment_pendingchain_result)
      );

      if (payment_pendingchain_result.length == 0) {
        await delay(10000);
        process.env.PAYMENT_REPLACE = false;
        process.env.PAYMENT_CHAINS_COMPARISON = false;
        console.log("finish payment chain comparison procedure");
        break;
      } else {
        var list_token = [];
        for (var i = 0; i < payment_pendingchain_result.length; i++) {
          var found = list_token.findIndex(
            (elementName) => elementName == payment_pendingchain_result[i].token
          );
          if (found == -1)
            list_token.push(payment_pendingchain_result[i].token);
        }
        //start
        console.log(list_token);
        var newChains = [];

        for (var i = 0; i < list_token.length; i++) {
          var targetChain = await auxiliary.rebuild_payment(
            list_token[i],
            payment_pendingchain_result
          );
          newChains.push(targetChain);
        }

        var trigger = false;

        var payment_result = await Payment.find({});
        payment_result = JSON.parse(JSON.stringify(payment_result));
        upDateChain = payment_result;
        for (var i = 0; i < newChains.length; i++) {
          if (
            newChains[i].length > upDateChain.length &&
            chainValidation(newChains[i])
          ) {
            console.log("detect new available payment chain");
            upDateChain = newChains[i];
            trigger = true;
          } else if (
            newChains[i].length == upDateChain.length &&
            chainValidation(newChains[i])
          ) {
            object1 = newChains[i];
            object2 = upDateChain;
            if (chainValidation(upDateChain)) {
              temp1 = object1[object1.length - 1].timestamp;
              temp2 = object2[object2.length - 1].timestamp;
              if (temp1 < temp2) {
                console.log("detect new available payment chain");
                upDateChain = newChains[i];
                trigger = true;
              }
            } else {
              console.log("detect new available payment chain");
              upDateChain = newChains[i];
              trigger = true;
            }
          }
        }

        if (trigger == true) {
          console.log("update_payment_chain");
          process.env.PAYMENT_REPLACE = true;

          var segmentLength = 200;
          var initLength_upDateChain = upDateChain.length;
          var numberThread = 0;
          var completeThread = 0;

          while (true) {
            await delay(1000);
            numberThread += 1;
            if (initLength_upDateChain > segmentLength) {
              initLength_upDateChain -= segmentLength;
              var cutupDateChain = upDateChain.slice(0, segmentLength);
              upDateChain = upDateChain.slice(segmentLength);
              var cutpayment_result = payment_result.slice(0, segmentLength);
              payment_result = payment_result.slice(segmentLength);

              var thread_update_paymentChain1_data = {
                validChain: cutupDateChain,
                insideDatabaseChain: cutpayment_result,
              };

              var worker = new Worker("./thread_update_paymentChain1.js", {
                workerData: thread_update_paymentChain1_data,
              });

              worker.on("exit", (code) => {
                completeThread += 1;
                console.log(`payment segment ${completeThread} is completed`);
              });
            } else {
              var thread_update_paymentChain1_data = {
                validChain: upDateChain,
                insideDatabaseChain: payment_result,
              };

              var worker = new Worker("./thread_update_paymentChain1.js", {
                workerData: thread_update_paymentChain1_data,
              });

              worker.on("exit", (code) => {
                completeThread += 1;
                console.log(`payment segment ${completeThread} is completed`);
              });

              break;
            }
          }

          while (true) {
            console.log("update payment");
            if (numberThread == completeThread) break;
            await delay(10000);
          }
        }

        //delete array in open_log_pendingchain
        for (var i = 0; i < list_token.length; i++) {
          await Payment_pendingchain.deleteMany(
            { token: list_token[i] },
            function (err, res) {}
          );
        }
      }
    }

    mongoose.disconnect();
  }
);

function chainValidation(chain) {
  var check = true;

  for (var i = 0; i < chain.length; i++) {
    if (chain[i].index == 0) {
      if (
        !(
          chain[i].previousHash == "0" &&
          chain[i].Hash ==
            SHA256(
              chain[i].pay_service +
                chain[i].pay_num +
                JSON.stringify(chain[i].pay_info) +
                chain[i].pay_date +
                chain[i].cost +
                chain[i].order_id +
                chain[i].nonce +
                chain[i].previousHash +
                chain[i].author
            ).toString()
        )
      ) {
        check = false;
        break;
      }
    } else {
      previousPosition = chain.findIndex(
        (element) => element.index == chain[i].index - 1
      );
      if (previousPosition == -1) {
        check = false;
        break;
      }
      previousBlock = chain[previousPosition];
      currentBlock = chain[i];
      if (
        !(
          currentBlock.previousHash == previousBlock.Hash &&
          currentBlock.Hash ==
            SHA256(
              chain[i].pay_service +
                chain[i].pay_num +
                JSON.stringify(chain[i].pay_info) +
                chain[i].pay_date +
                chain[i].cost +
                chain[i].order_id +
                chain[i].nonce +
                chain[i].previousHash +
                chain[i].author
            ).toString()
        )
      ) {
        check = false;
        break;
      }
    }
  }

  console.log(`validate payment chain: ${check}`);
  if (check) return true;
  else return false;
}
