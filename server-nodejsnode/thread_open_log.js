const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_open_log");
console.log("New open_log");
console.log(workerData);
mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.open_log(
      workerData.user_id,
      workerData.box_id,
      workerData.station_id,
      workerData.open_time
    );
    mongoose.disconnect();
  }
);
