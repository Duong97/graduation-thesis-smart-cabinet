var mongoose = require("mongoose");
var nodeInteract = require("./nodeInteract");
var blockchain = require("./blockchain");
const delay = require("delay");

var url = "ADD YOUR DATABASE ADDRESS HERE!";
process.env.URL = url;
process.env.DATABASE = "dhd2";
process.env.NODE_NAME = "node2";

var socket = require("socket.io-client")("ADD YOUR SERVER ADDRESS HERE!");

mongoose.connect(
  url,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  function (err, mongoose_result) {
    if (err) console.log("Unable to connect to the mongoDB server.Error", err);
    else {
      console.log(
        "Connected to MongoDB Server, WebService running on port 3000"
      );

      console.log(`Name of node: ${process.env.NODE_NAME}`);

      socket.on("for_node", async function (data) {
        if (data.message == "I see you") {
          socket.disconnect();
          await delay(1000);
          socket.connect();
        }
      });

      blockchain.order_broadcast(socket);
      blockchain.order_pendingchain_broadcast(socket);

      blockchain.payment_broadcast(socket);
      blockchain.payment_pendingchain_broadcast(socket);

      blockchain.open_log_broadcast(socket);
      blockchain.open_log_pendingchain_broadcast(socket);

      blockchain.authorize_log_broadcast(socket);
      blockchain.authorize_log_pendingchain_broadcast(socket);

      nodeInteract.Internal(socket);
    }
  }
);
