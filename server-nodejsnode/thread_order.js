const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_order");
console.log(workerData);
mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.order(
      workerData.order_id,
      workerData.user_id,
      workerData.box_id,
      workerData.station_id,
      workerData.start_time,
      workerData.end_time,
      workerData.order_time
    );

    mongoose.disconnect();
  }
);
