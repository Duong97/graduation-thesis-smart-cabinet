package com.example.demorasp1;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.net.URISyntaxException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import android.widget.TextView;

import android.graphics.Bitmap;

import android.widget.ImageView;
import android.widget.VideoView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Map;
import java.util.TimeZone;
import android.os.CountDownTimer;

import static android.content.ContentValues.TAG;
import static xdroid.toaster.Toaster.toast;
//import static xdroid.toaster.Toaster.toastLong;

/**
 * Skeleton of an Android Things activity.
 * <p>
 * Android Things peripheral APIs are accessible through the class
 * PeripheralManagerService. For example, the snippet below will open a GPIO pin and
 * set it to HIGH:
 *
 * <pre>{@code
 * PeripheralManagerService service = new PeripheralManagerService();
 * mLedGpio = service.openGpio("BCM6");
 * mLedGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
 * mLedGpio.setValue(true);
 * }</pre>
 * <p>
 * For more complex peripherals, look for an existing user-space driver, or implement one if none
 * is available.
 *
 * @see <a href="https://github.com/androidthings/contrib-drivers#readme">https://github.com/androidthings/contrib-drivers#readme</a>
 */

public class MainActivity extends Activity {
    UsbDeviceConnection connection;
    UsbSerialDevice serial;
    UsbManager usbManager;

    ImageView imageView;
    public final static int QRcodeWidth = 500 ;
    Bitmap bitmap ;

    String station_id = "5edb615fa6568321887ed060";
    String token;
    int amount_cabinet = 4;
    ArrayList<box_info> box_used = new ArrayList<>();
    CountDownTimer[] box_countDown = new CountDownTimer[amount_cabinet] ;

    private TextView place_display;
    String place;
    private TextView location_display;
    String location;
    private TextView no_display;
    String no;

    public Socket mSocket;
    public String source1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        //take resolution of display
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        Log.d("W", String.valueOf(width));
        Log.d("H", String.valueOf(height));

        //Register USB serial
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        Map<String, UsbDevice> connectedDevices = usbManager.getDeviceList();
        for (UsbDevice device : connectedDevices.values()) {
            if (device.getVendorId() == 6790 ) {
                Log.i(TAG, "Device found: " + device.getDeviceName());
                startSerialConnection(usbManager, device);
                break;
            }
        }

        //initialize server connection
        try {
            mSocket = IO.socket("ADD YOUR SERVER ADDRESS HERE!");

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        imageView = (ImageView)findViewById(R.id.imageView);
        place_display = (TextView)findViewById(R.id.Place);
        location_display = (TextView)findViewById(R.id.Location);
        no_display = (TextView)findViewById(R.id.No);

        //Connect to the server
        mSocket.connect();
        mSocket.emit("client",station_id); // announce to server that station is connected
        mSocket.emit("station_info",station_id);

        //Listen message from server
        mSocket.on("for_station", server_restart);// server restarts
        mSocket.on("station_info", station_info);
        mSocket.on("server", onRetrieData);// server confirms that station has been connected
        mSocket.on(station_id, onRetrieData1); // receive box number to open
        mSocket.on("info_initial", onRetrieData2);// receive info of each box which is used when station is boot up
        mSocket.on("delete_result", onRetrieData3);// receive result of delete
        mSocket.on("info_update", onRetrieData4);// receive new info of box which is registered already

        Thread task = new Thread()
        {
            @Override
            public void run ()
            {
                while(true)
                {
                    SecureRandom secureRandom = new SecureRandom(); //threadsafe
                    Base64.Encoder base64Encoder = null; //threadsafe
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        base64Encoder = Base64.getUrlEncoder();
                    }

                    byte[] randomBytes = new byte[12];
                    secureRandom.nextBytes(randomBytes);
                    token = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        token = base64Encoder.encodeToString(randomBytes);
                    }

                    try {
                        bitmap = TextToImageEncode(token);
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("token", token);
                            imageView.setImageBitmap(bitmap);
                            // Stuff that updates the UI
                        }
                    });

                    JSONObject input = new JSONObject();
                    try {
                        input.put("id", station_id);
                        input.put("token", token);
                        mSocket.emit("token",input);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(60000*3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        };

        source1 = "android.resource://" + getPackageName() + "/" + R.raw.vn;
        //source1 = "https://live.vtvdigital.vn/hls/037fa86352648d7d270371f68b077a9d/1583301384633/live/_definst_/vtv1-high.m3u8";
        final VideoView videoView1 = (VideoView) findViewById(R.id.videoView1);
        videoView1.setVideoPath(source1);

        Thread task1 = new Thread()
        {
            @Override
            public void run ()
            {
                while(true)
                {
                    if (videoView1.isPlaying())
                    {
                        try {
                            Thread.sleep(15000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        videoView1.start();
                    }
                }
            }
        };

        task.start();
        task1.start();
    }

    private Emitter.Listener server_restart = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = (JSONObject) args[0];
                    try {
                        String name = obj.getString("message");
                        if (name.equals("I see you"))
                        {
                            mSocket.disconnect();
                            Thread.sleep(1000);
                            mSocket.connect();
                            Thread.sleep(1000);
                            mSocket.emit("client",station_id);
                            mSocket.emit("station_info",station_id);
                            Thread.sleep(1000);
                            JSONObject input = new JSONObject();
                            try {
                                input.put("id", station_id);
                                input.put("token", token);
                                mSocket.emit("token",input);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("Message", "server restart");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener station_info = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = (JSONObject) args[0];
                    try {
                        location = obj.getString("location");
                        location_display.setText("Location: " + location);
                        no = obj.getString("no");
                        no_display.setText("No: " + no);
                        place = obj.getString("placename");
                        place_display.setText("Place: " + place);
                        Log.d("Station_info", obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    //server confirms that station has been connected
    private Emitter.Listener onRetrieData = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = (JSONObject) args[0];
                    try {
                        String name = obj.getString("message");
                        Toast.makeText(MainActivity.this, name,Toast.LENGTH_SHORT).show();
                        Log.d("Message", name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    //receive box number to open
    private Emitter.Listener onRetrieData1 = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final JSONObject obj = (JSONObject) args[0];
                    new Thread(){
                        @Override
                        public void run ()
                        {
                            try {
                                int box_no = Integer.parseInt(obj.getString("box"));
                                int box_on_circuit = box_no+1;
                                byte[] send_data = new byte[]{(byte) 0x8a, (byte) 1, (byte) box_on_circuit, (byte) 0x11, (byte) 0x00};
                                byte CRC = (byte) (send_data[0] ^ send_data[1] ^ send_data[2] ^ send_data[3]);
                                send_data[4] = CRC;
                                serial.write(send_data);
                                toast(String.valueOf(box_no));
                                Log.d("Open box", String.valueOf(box_no));




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
            });
        }
    };

    // receive info of each box which is used when station is boot up
    private Emitter.Listener onRetrieData2 = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    box_used.clear();
                    try {
                        JSONArray convertListBox = new JSONArray(args[0].toString());

                        for (int i = 0; i < convertListBox.length(); i++){
                            JSONObject inside_convertListBox = new JSONObject(convertListBox.getString(i));
                            JSONObject inside_cabinet= new JSONObject(inside_convertListBox.getString("box_id"));

                            final String box_id = inside_cabinet.getString("_id");
                            final String box_no = inside_cabinet.getString("no");
                            String box_starttime = inside_convertListBox.getString("start_time");
                            String box_endtime = inside_convertListBox.getString("end_time");

                            DateFormat dateFormat = new SimpleDateFormat("MM dd yy HH:mm");
                            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                            Date d1 = dateFormat.parse(box_endtime);
                            Date d2 = new Date();

                            Log.d("Diffrence",String.valueOf(d1.getTime() - d2.getTime()));

                            if (d1.compareTo(d2)<= 0){
                                JSONObject input = new JSONObject();
                                try {
                                    input.put("box_no", box_no);
                                    input.put("box_id", box_id);
                                    input.put("station_id", station_id);
                                    mSocket.emit("box_expired",input);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else {
                                final box_info temp = new box_info();
                                temp.box_id = box_id;
                                temp.box_no = box_no;
                                temp.box_starttime = box_starttime;
                                temp.box_endtime = box_endtime;
                                final int index = i;
                                if (box_countDown[Integer.parseInt(box_no)] != null){
                                    box_countDown[Integer.parseInt(box_no)].cancel();
                                }

                                box_countDown[Integer.parseInt(box_no)] = new CountDownTimer(d1.getTime() - d2.getTime(), 60000) {
                                    public void onTick(long millisUntilFinished) {
                                        Log.d("Box "+ box_no,"Minutes remaining: " +String.valueOf(millisUntilFinished / 60000));
                                    }

                                    public void onFinish() {
                                        JSONObject input = new JSONObject();
                                        try {
                                            input.put("box_no", temp.box_no);
                                            input.put("box_id", temp.box_id);
                                            input.put("station_id", station_id);
                                            mSocket.emit("box_expired",input);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        Log.d("Box "+ box_no,"done!");
                                        box_used.remove(temp);
                                    }
                                }.start();
                                box_used.add(temp);
                            }
                        }

                        //initialize check status of cabinets
                        for (int i = 0; i < amount_cabinet; i++) {
                            try {
                                int box_on_circuit = i+1;
                                byte[] send_data = new byte[]{(byte) 0x80, (byte) 1, (byte) box_on_circuit, (byte) 0x33, (byte) 0x00};
                                byte CRC = (byte) (send_data[0] ^ send_data[1] ^ send_data[2] ^ send_data[3]);
                                send_data[4] = CRC;
                                serial.write(send_data);
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    //receive result of delete
    private Emitter.Listener onRetrieData3 = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = (JSONObject) args[0];
                    Log.d("delete_result", obj.toString());
                    try {
                        String box_id_trans = obj.getString("box_id");
                        String box_no_trans = obj.getString("box_no");
                        String station_id_trans = obj.getString("station_id");

                        if (station_id_trans.equals(station_id)){
                            for (int i = 0; i < box_used.size(); i++) {
                                if (box_used.get(i).box_id.equals(box_id_trans)) {
                                    box_used.remove(i);
                                    break;
                                }
                            }
                            if (box_countDown[Integer.parseInt(box_no_trans)] != null){
                                box_countDown[Integer.parseInt(box_no_trans)].cancel();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    // receive new info of box which has been registered already
    private Emitter.Listener onRetrieData4 = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = (JSONObject) args[0];
                    try {
                        String station_idTrans = obj.getString("station_id");
                        if (station_idTrans.equals(station_id)){
                            final String box_id = obj.getString("box_id");
                            final String box_no = obj.getString("box_no");
                            String box_starttime = obj.getString("start_time");
                            String box_endtime = obj.getString("end_time");

                            DateFormat dateFormat = new SimpleDateFormat("MM dd yy HH:mm");
                            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                            Date d1 = dateFormat.parse(box_endtime);
                            Date d2 = new Date();

                            Log.d("Diffrence",String.valueOf(d1.getTime() - d2.getTime()));

                            final box_info temp = new box_info();
                            temp.box_id = box_id;
                            temp.box_no = box_no;
                            temp.box_starttime = box_starttime;
                            temp.box_endtime = box_endtime;
                            final int index = box_used.size();
                            if (box_countDown[Integer.parseInt(box_no)] != null){
                                box_countDown[Integer.parseInt(box_no)].cancel();
                            }

                            box_countDown[Integer.parseInt(box_no)] = new CountDownTimer(d1.getTime() - d2.getTime(), 60000) {

                                public void onTick(long millisUntilFinished) {
                                    Log.d("Box "+ box_no,"Minutes remaining: " +String.valueOf(millisUntilFinished / 60000));
                                }

                                public void onFinish() {
                                    JSONObject input = new JSONObject();
                                    try {
                                        input.put("box_no", box_no);
                                        input.put("box_id", box_id);
                                        input.put("station_id", station_id);
                                        mSocket.emit("box_expired",input);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("Box "+ box_no,"done!");
                                    box_used.remove(temp);
                                }
                            }.start();
                            box_used.add(temp);
                            Log.d("New box",String.valueOf(temp));
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    public void checkClose(final int duration, final int no){
        new CountDownTimer(duration, 1000) {
            public void onTick(long millisUntilFinished) {
                //Log.d("Timer "+ String.valueOf(index),"seconds remaining: " +String.valueOf(millisUntilFinished / 1000));
            }
            public void onFinish() {
                int box_on_circuit = no+1;
                byte[] send_data = new byte[]{(byte) 0x80, (byte) 1, (byte) box_on_circuit, (byte) 0x33, (byte) 0x00};
                byte CRC = (byte) (send_data[0] ^ send_data[1] ^ send_data[2] ^ send_data[3]);
                send_data[4] = CRC;
                serial.write(send_data);
                Log.d("Check Close", "check close");
            }
        }.start();
    }

    public void update_status (int[] component)
    {
        int no = component[2]-1;
        int status;

        Log.d("here", "update_status");

        if (component[3] == 17)
        {
            status = 0;
            for (int j = 0; j < box_used.size(); j ++) {
                if (String.valueOf(no).equals(box_used.get(j).box_no)) {
                    Log.d("found","found");
                    JSONObject input2 = new JSONObject();
                    try {
                        input2.put("station_id", station_id);
                        input2.put("box_no", no);
                        input2.put("status", status);
                        //Thread.sleep(3000);
                        mSocket.emit("not_close",input2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    checkClose(30000, no);
                    break;
                }
            }
        }
        else
            status = 1;

        JSONObject input = new JSONObject();
        try {
            input.put("station_id", station_id);
            input.put("box_no", no);
            input.put("status", status);
            //Thread.sleep(3000);
            mSocket.emit("update_status",input);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );
        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];
        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor):getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    void startSerialConnection(UsbManager usbManager, UsbDevice device) {
        connection = usbManager.openDevice(device);
        serial= UsbSerialDevice.createUsbSerialDevice(device, connection);
        if (serial != null && serial.open()) {
            serial.setBaudRate(9600);
            serial.setDataBits(UsbSerialInterface.DATA_BITS_8);
            serial.setStopBits(UsbSerialInterface.STOP_BITS_1);
            serial.setParity(UsbSerialInterface.PARITY_NONE);
            serial.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
            serial.read(mCallback);
        }
    }

    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
        @Override
        public void onReceivedData(final byte[] data) {
            final int[] component = new int[5];
            for (int i = 0; i < data.length; i++) {
                component[i] = Integer.parseInt(String.format("%02X", data[i]), 16);
                Log.d("response", String.valueOf(component[i]));
            }

            if (component[0] == 128)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        update_status(component);
                    }
                });
            }
        }
    };

    private void stopUsbConnection() {
        try {
            if (serial != null) {
                serial.close();
            }
            if (connection != null) {
                connection.close();
            }
        } finally {
            serial = null;
            connection = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopUsbConnection();
        mSocket.close();
    }
}