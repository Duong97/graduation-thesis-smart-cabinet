const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const AuthorizeLogPendingChainSchema = new Schema({
  chain: {
    type: Array,

    required: true,
  },

  from: {
    type: String,

    required: true,
  },

  token: {
    type: String,

    required: true,
  },
  index: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "authorize_log_pendingchain",
  AuthorizeLogPendingChainSchema,
  "authorize_log_pendingchain"
);
