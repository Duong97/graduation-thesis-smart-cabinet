const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./user");
require("./cabinet");
require("./station");
// Create Schema
const OpenLogPendingSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,
    ref: "cabinet",
    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,
    ref: "station",
    required: true,
  },

  open_time: {
    type: String,
    required: true,
  },

  process: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "open_log_pending",
  OpenLogPendingSchema,
  "open_log_pending"
);
