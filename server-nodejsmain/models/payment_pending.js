const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./order");
// Create Schema
const PaymentPendingSchema = new Schema({
  pay_service: {
    type: String,
    required: true,
  },

  pay_num: {
    type: String,
    required: true,
  },

  pay_info: {
    customerNumber: String,
  },

  pay_date: {
    type: String,
    required: true,
  },

  cost: { type: String, required: true },

  order_id: { type: String, ref: "order", required: true },

  process: { type: Number, required: true },
});
module.exports = mongoose.model(
  "payment_pending",
  PaymentPendingSchema,
  "payment_pending"
);
