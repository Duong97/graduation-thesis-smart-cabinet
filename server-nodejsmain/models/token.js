const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./station");
// Create Schema
const TokenSchema = new Schema({
  station_id: {
    type: Schema.Types.ObjectId,
    ref: "station",
    required: true,
  },

  token: {
    type: String,
    required: true,
  },
});
module.exports = mongoose.model("token", TokenSchema, "token");
