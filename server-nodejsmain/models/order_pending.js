const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./user");
require("./cabinet");
require("./station");
// Create Schema
const OrderPendingSchema = new Schema({
  order_id: {
    type: String,
    required: true,
  },
  user_id: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  box_id: {
    type: Schema.Types.ObjectId,
    ref: "cabinet",
    required: true,
  },

  station_id: {
    type: Schema.Types.ObjectId,
    ref: "station",
    required: true,
  },

  start_time: {
    type: String,
    required: true,
  },

  end_time: {
    type: String,
    required: true,
  },

  order_time: {
    type: String,
    required: true,
  },

  process: {
    type: Number,
    required: true,
  },
});
module.exports = mongoose.model(
  "order_pending",
  OrderPendingSchema,
  "order_pending"
);
