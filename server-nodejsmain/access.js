var crypto = require("crypto");
var User = require("./models/user");
const { request } = require("http");

var genRandomString = function (length) {
  return crypto
    .randomBytes(Math.ceil(length / 2))
    .toString("hex")
    .slice(0, length);
};

var sha512 = function (password, salt) {
  var hash = crypto.createHmac("sha512", salt);
  hash.update(password);
  var value = hash.digest("hex");
  return {
    salt: salt,
    passwordHash: value,
  };
};

function saltHashPassword(userPassword) {
  var salt = genRandomString(16);
  var passwordData = sha512(userPassword, salt);
  return passwordData;
}

function checkHashPassword(userPassword, salt) {
  var passwordData = sha512(userPassword, salt);
  return passwordData;
}

exports.Internal = (io, app) => {
  //login_account
  app.post("/login", async (request, response, next) => {
    var post_data = request.body;
    var email = post_data.email;
    var userPassword = post_data.password;

    console.log(email);
    console.log(userPassword);

    User.findOne({ email: email }, async function (err, user) {
      user = JSON.parse(JSON.stringify(user));
      if (user == null) {
        response.json("Email not exists");
        console.log("Email not exists");
      } else {
        if (user.active) {
          if (!user.loginState) {
            var salt = user.salt;
            var hashed_password = checkHashPassword(userPassword, salt)
              .passwordHash;
            var encrypted_password = user.password;
            if (hashed_password == encrypted_password) {
              // //new
              // if (user.birthday != null) {
              //   var temp = new Date(user.birthday);
              //   var transBirthday =
              //     temp.getDate().toString() +
              //     "/" +
              //     (temp.getMonth() + 1).toString() +
              //     "/" +
              //     temp.getFullYear().toString();
              //   user.birthday = transBirthday;
              // }
              response.json(user);
              console.log("Login success");
              console.log("username:" + user.username);
              console.log("id:" + user._id);
              // User.updateOne({ _id: user._id }, { loginState: true }, function (
              //   err,
              //   user_res
              // ) {});
            } else {
              response.json("Wrong password");
              console.log("Wrong password");
            }
          } else {
            response.json("Someone is using this account");
            console.log("Someone is using this account");
          }
        } else {
          response.json("Access denied!");
          console.log("Access denied!");
        }
      }
    });
  });

  //register_account
  app.post("/register", (request, response, next) => {
    var post_data = request.body;
    var plaint_password = post_data.password;
    var hash_data = saltHashPassword(plaint_password);
    var password = hash_data.passwordHash;
    var salt = hash_data.salt;
    var username = post_data.username;
    var email = post_data.email;
    var insertJson = {
      username: username,
      email: email,
      password: password,
      salt: salt,
    };

    User.findOne({ $or: [{ username: username }, { email: email }] }, function (
      err,
      user
    ) {
      if (user != null) {
        response.json("Account exists");
        console.log("Account exists");
      } else {
        User(insertJson).save();
        response.json("Registration success");
        console.log("Registration success");
      }
    });
  });

  //update_account
  app.post("/update_info", (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;
    var username = post_data.username;
    var sex = post_data.sex;
    var birthday = post_data.birthday;
    var phone_number = post.data.phone_number;

    User.updateOne(
      { _id: user_id },
      {
        username: username,
        sex: sex,
        birthday: new Date(birthday),
        phone_number: phone_number,
      },
      (err, result) => {
        response.json({ message: "ok" });
      }
    );
  });

  //query_account
  app.post("/user_info", (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;

    User.findOne({ _id: user_id }, async function (err, user_result) {
      // //new
      // if (user_result.birthday != null) {
      //   var temp = new Date(user_result.birthday);
      //   var transBirthday =
      //     temp.getDate().toString() +
      //     "/" +
      //     (temp.getMonth() + 1).toString() +
      //     "/" +
      //     temp.getFullYear().toString();
      //   user_result.birthday = transBirthday;
      // }

      response.json(user_result);
    });
  });

  //query station id
  app.post("/query_station_id", async (request, response) => {
    var post_data = request.body;
    var token = post_data.token;

    var Token = require("./models/token");
    var token_result = await Token.findOne({ token: token });
    console.log(token_result);
    if (token_result != null) {
      response.json({ station_id: token_result.station_id });
    } else {
      response.json({ station_id: null });
    }
  });

  //logout
  app.post("/logout", async (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;

    User.updateOne({ _id: user_id }, { loginState: false }, function (
      err,
      user_res
    ) {});

    response.json("logout");
  });

  //User broadcast
  this.user_broadcast(io, app);
};

exports.user_broadcast = (io, app) => {
  //listen mongodb
  console.log("user_broadcast");
  User.watch().on("change", async function (result) {
    var document_id = result.documentKey._id;
    var user_result = await User.findOne({
      _id: document_id,
    });
    if (user_result != null) {
      if (!user_result.active) {
        // User.updateOne(
        //   { _id: document_id },
        //   { online: false, loginState: false },
        //   function (err, user_result) {}
        // );

        var transfer = {
          user_id: user_result._id,
          active: user_result.active,
        };
        io.sockets.emit("blockUser", transfer);
      }
    }
  });
};
