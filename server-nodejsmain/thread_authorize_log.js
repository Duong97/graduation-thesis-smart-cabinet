const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_authorize_log");
console.log("New authorize_log");

mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.authorize_log(
      workerData.owner_id,
      workerData.authorize_id,
      workerData.box_id,
      workerData.station_id,
      workerData.limit,
      workerData.action,
      workerData.authorize_time
    );

    mongoose.disconnect();
  }
);
