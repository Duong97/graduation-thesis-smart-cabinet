var nodeInteract = require("./nodeInteract");
const { Worker } = require("worker_threads");
const delay = require("delay");

var list_user = [];
var Station = require("./models/station");

var Own = require("./models/own");

var Token = require("./models/token");

var Use = require("./models/use");

var Authorize = require("./models/authorize");

var Cabinet = require("./models/cabinet");

var User = require("./models/user");

exports.hardware = (io, app) => {
  io.sockets.emit("for_node", { message: "I see you" });
  io.sockets.emit("for_station", { message: "I see you" });
  io.sockets.emit("for_mobile", { message: "I see you" });
  io.sockets.on("connection", function (socket) {
    var socket_id = socket.id;
    console.log(`Socket connected, ID: ${socket_id}`);
    socket.emit("server", { message: "I see you" });
    socket.emit("socket_id", socket_id);

    //For node in network
    nodeInteract.Internal(io, socket);

    //For mobile device
    socket.on("mobile_client", async function (data) {
      console.log("user_id " + data.user_id + " is online");
      User.updateOne({ _id: data.user_id }, { online: true }, function (
        err,
        user_result
      ) {});

      var found = list_user.findIndex((user) => user.user_id == data.user_id);
      if (found == -1) {
        list_user.push(data);
      } else {
        list_user[found].socket_id = data.socket_id;
      }

      console.log(`Number connected user is ${list_user.length}`);
    });

    socket.on("exit_app", function (data) {
      console.log("user_id " + data.user_id + " is offline");
      User.updateOne({ _id: data.user_id }, { online: false }, function (
        err,
        user_result
      ) {});
    });

    socket.on("disconnect", async function (data) {
      var found = list_user.findIndex((user) => user.socket_id == socket_id);
      if (found != -1) {
        var user_result = await User.findOne({ _id: list_user[found].user_id });
        if (user_result.online) {
          console.log("user_id " + user_result._id + " is offline");
          User.updateOne({ _id: user_result._id }, { online: false }, function (
            err,
            user_result
          ) {});
        }
        list_user.splice(found, 1);
        console.log(`Number connected user is ${list_user.length}`);
      }
    });

    //For station
    socket.on("station_info", function (data) {
      Station.findOne({ _id: data }, function (err, station_result) {
        if (err) throw err;
        socket.emit("station_info", station_result);
      });
    });

    socket.on("client", function (data) {
      console.log("station_id " + data + " is connected!");

      Own.find({ station_id: data })
        .populate("box_id")
        .populate("station_id")
        .exec(function (err, own_result) {
          if (err) throw err;

          socket.emit("info_initial", own_result);
        });
    });

    socket.on("token", function (data) {
      console.log("id: " + data.id + " || token: " + data.token);

      var updateJson = {
        station_id: data.id,
      };

      var newvalues = { token: data.token };

      Token.findOne(updateJson, function (err, res) {
        if (res == null) {
          Token({ station_id: data.id, token: data.token }).save();
          console.log(`Initial set up token for station_if ${data.id}`);
        } else
          Token.updateOne(updateJson, newvalues, function (err, res) {
            console.log("Token Updating...");
          });
      });
    });

    socket.on("box_expired", function (data) {
      var box_no = data.box_no;
      var box_id = data.box_id;
      var station_id = data.station_id;

      console.log(
        "box_no: " +
          box_no +
          " || box_id: " +
          box_id +
          " || station_id: " +
          station_id
      );
      console.log("box_expired...");

      var deleteJsonUse = {
        box_id: box_id,
        station_id: station_id,
      };

      Use.deleteMany(deleteJsonUse, function (err, res) {});

      var deleteJsonAuth = {
        box_id: box_id,
        station_id: station_id,
      };

      Authorize.deleteMany(deleteJsonAuth, function (err, res) {});

      var deleteJsonOwn = {
        box_id: box_id,
        station_id: station_id,
      };

      Own.deleteOne(deleteJsonOwn, function (err, res) {});

      console.log("Delete success");

      var updateJson = {
        _id: box_id,
        station_id: station_id,
      };

      var newvalues = { state: 0 };

      Cabinet.updateOne(updateJson, newvalues, function (err, res) {
        console.log("Update success");
      });
    });

    socket.on("update_status", function (data) {
      console.log(
        "station_id: " +
          data.station_id +
          " || No: " +
          data.box_no +
          " || Status: " +
          data.status
      );

      var updateJson = {
        no: data.box_no,
        station_id: data.station_id,
      };

      var newvalues = { status: data.status };

      Cabinet.updateOne(updateJson, newvalues, function (err, res) {
        console.log("State Updating...");
      });
    });

    socket.on("not_close", async function (data) {
      console.log(
        "station_id: " +
          data.station_id +
          " || No: " +
          data.box_no +
          " || Status: " +
          data.status
      );

      var box_info = await Cabinet.findOne({
        $and: [{ no: data.box_no }, { station_id: data.station_id }],
      });

      var station_info = await Station.findOne({
        _id: data.station_id,
      });

      if (box_info != null && station_info != null) {
        Use.find(
          {
            $and: [{ station_id: data.station_id }, { box_id: box_info._id }],
          },

          function (err, result) {
            for (var i = 0; i < result.length; i++) {
              var temp = {
                user_id: result[i].user_id,
                box_id: result[i].box_id,
                box_no: box_info.no,
                station_id: result[i].station_id,
                station_location: station_info.location,
                station_no: station_info.no,
              };
              io.sockets.emit("not_close", temp);
            }
          }
        );
      }
    });
  });
};

exports.manipulate_box = async (io, app) => {
  //Load_station_of_user
  app.post("/load_station", async (request, response, next) => {
    var post_data = request.body;
    var id_num = post_data.id_num;

    console.log("load_station");

    Use.find({ user_id: id_num })
      .populate("box_id")
      .populate("station_id")
      .exec(function (err, use) {
        if (err) throw err;

        response.json(use);
      });
  });

  //Load_station of user base on search key
  app.post("/load_station_search_key", async (request, response, next) => {
    await delay(100);
    var post_data = request.body;
    var user_id = post_data.user_id;
    var search_data = post_data.search_data;
    var use_result = await Use.find({ user_id: user_id });

    var result = [];

    for (var i = 0; i < use_result.length; i++) {
      var station_result = await Station.findOne({
        $and: [
          { _id: use_result[i].station_id },
          {
            $or: [
              {
                location: {
                  $regex: new RegExp("^" + search_data + ".*", "i"),
                },
              },
              {
                placename: {
                  $regex: new RegExp("^" + search_data + ".*", "i"),
                },
              },
              {
                address: { $regex: new RegExp("^" + search_data + ".*", "i") },
              },
            ],
          },
        ],
      }).populate("price_id");

      station_result = JSON.parse(JSON.stringify(station_result));

      if (station_result != null) {
        var found = result.findIndex(
          (elementID) => elementID._id == station_result._id
        );

        if (found == -1) {
          result.push(station_result);
        }
      }
    }

    response.json(result);
  });

  //Open_box
  app.post("/open_box", async (request, response, next) => {
    var post_data = request.body;
    var id_num = post_data.id_num;
    var box_id = post_data.box_id;
    var box_no = post_data.box_no;
    var token = post_data.token;
    var currentTime = new Date().toString();

    Token.findOne({ token: token }, function (err, result) {
      //find station id which is mapped to token
      if (result == null) {
        response.json("Token expired");
      } else {
        result = JSON.parse(JSON.stringify(result));
        var station_id = result.station_id;

        Use.findOne(
          {
            $and: [
              { user_id: id_num },
              { box_id: box_id },
              { station_id: station_id },
            ],
          },
          function (err, use_result) {
            //Check unlock right of user
            if (use_result == null) {
              response.json("Access denied!");
            } else {
              //For owner
              if (use_result.role == "own") {
                console.log(
                  "Opening box " + box_no + " at station_id " + station_id
                );
                io.sockets.emit(station_id, { box: box_no });

                response.json("Opening");
                var thread_open_log_data = {
                  id_num: id_num,
                  box_id: box_id,
                  station_id: station_id,
                  currentTime: currentTime,
                };

                new Worker("./thread_open_log.js", {
                  workerData: thread_open_log_data,
                });
                //blockchain.open_log(id_num, box_id, station_id, currentTime);
              }
              //For authority user
              else if (use_result.role == "auth") {
                //unlimited using
                if (use_result.limit == "unlimited") {
                  console.log(
                    "Opening box " + box_no + " at station_id " + station_id
                  );
                  io.sockets.emit(station_id, { box: box_no });

                  response.json("Opening");
                  var thread_open_log_data = {
                    id_num: id_num,
                    box_id: box_id,
                    station_id: station_id,
                    currentTime: currentTime,
                  };
                  new Worker("./thread_open_log.js", {
                    workerData: thread_open_log_data,
                  });
                }
                //limited using
                else {
                  var currentLimit = Number(use_result.limit);
                  //Still in bounds of using
                  if (currentLimit > 0) {
                    console.log(
                      "Opening box " + box_no + " at station_id " + station_id
                    );
                    io.sockets.emit(station_id, { box: box_no });

                    response.json("Opening");
                    var thread_open_log_data = {
                      id_num: id_num,
                      box_id: box_id,
                      station_id: station_id,
                      currentTime: currentTime,
                    };
                    new Worker("./thread_open_log.js", {
                      workerData: thread_open_log_data,
                    });

                    var updateJson = {
                      user_id: id_num,
                      box_id: box_id,
                      station_id: station_id,
                    };

                    var temp = currentLimit - 1;

                    var newvalues = { limit: temp.toString() };

                    Use.updateOne(updateJson, newvalues, function (err, res) {
                      console.log("Limit Updating...");
                    });
                  }
                  //Out of using limit
                  else {
                    response.json("Out of using limit");
                  }
                }
              }
            }
          }
        );
      }
    });
  });

  //delete_a_box
  app.post("/delete_a_box", (request, response, next) => {
    var post_data = request.body;
    var id_num = post_data.id_num;
    var box_id = post_data.box_id;
    var box_no = post_data.box_no;
    var station_id = post_data.station_id;

    Own.findOne(
      {
        $and: [
          { station_id: station_id },
          { user_id: id_num },
          { box_id: box_id },
        ],
      },
      function (err, own_result) {
        if (own_result == null) {
          response.json("Box is already deleted");
          console.log("Box is already deleted");
        } else {
          var deleteJsonUse = {
            box_id: box_id,
            station_id: station_id,
          };

          Use.deleteMany(deleteJsonUse, function (err, res) {});

          var deleteJsonAuth = {
            box_id: box_id,
            station_id: station_id,
          };

          Authorize.deleteMany(deleteJsonAuth, function (err, res) {});

          var deleteJsonOwn = {
            user_id: id_num,
            box_id: box_id,
            station_id: station_id,
          };

          Own.deleteOne(deleteJsonOwn, function (err, res) {});
          response.json("Delete success");
          console.log("Delete success");

          var updateJson = {
            _id: box_id,
            station_id: station_id,
          };

          var newvalues = { state: 0 };
          Cabinet.updateOne(updateJson, newvalues, function (err, res) {
            console.log("Update success");
          });

          var delete_resultJson = {
            box_id: box_id,
            box_no: box_no,
            station_id: station_id,
          };

          io.sockets.emit("delete_result", delete_resultJson);
        }
      }
    );
  });
};
