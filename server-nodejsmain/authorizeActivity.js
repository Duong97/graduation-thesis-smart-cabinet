const { Worker } = require("worker_threads");
var User = require("./models/user");
var Contact = require("./models/contact");
var Use = require("./models/use");
var Authorize = require("./models/authorize");

exports.Internal = (io, app) => {
  //download user data for searching
  app.post("/list_user", (request, response, next) => {
    var post_data = request.body;
    var user_id = post_data.user_id;
    var search_data = post_data.search_data;

    User.find({
      $and: [
        { _id: { $ne: user_id } },
        { email: new RegExp("^" + search_data + ".*") },
      ],
    })
      .select({ password: 0, salt: 0 })
      .exec(function (err, result) {
        response.json(result);
      });
  });

  //list contact of user
  app.post("/list_contact", (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;

    Contact.find({ user_id: user_id })
      .populate("contact_id", { password: 0, salt: 0 })
      .exec(function (err, result) {
        if (err) throw err;
        response.json(result);
      });
  });

  //add a contact
  app.post("/add_a_contact", (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;
    var contact_id = post_data.contact_id;
    console.log("add_a_contact");

    Contact.findOne({ user_id: user_id, contact_id: contact_id }, function (
      err,
      contact_result
    ) {
      if (contact_result == null) {
        var insertJson = {
          user_id: user_id,
          contact_id: contact_id,
        };

        Contact(insertJson).save();
      }
    });

    response.json("done");
  });

  //delete a contact
  app.post("/delete_a_contact", (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;
    var contact_id = post_data.contact_id;
    var currentTime = new Date().toString();

    Contact.findOne({ user_id: user_id, contact_id: contact_id }, function (
      err,
      contact_result
    ) {
      if (contact_result == null) {
        response.json("Contact is already deleted");
        console.log("Contact is already deleted");
      } else {
        Contact.deleteOne(
          { user_id: user_id, contact_id: contact_id },
          function (err, contact_result1) {}
        );

        Authorize.find(
          {
            owner_id: user_id,
            authorize_id: contact_id,
          },
          async function (err, authorize_result) {
            authorize_result = JSON.parse(JSON.stringify(authorize_result));

            for (var i = 0; i < authorize_result.length; i++) {
              Use.deleteOne(
                {
                  user_id: authorize_result[i].authorize_id,
                  box_id: authorize_result[i].box_id,
                  station_id: authorize_result[i].station_id,
                },
                function (err, res) {}
              );

              Authorize.deleteOne(
                {
                  owner_id: authorize_result[i].owner_id,
                  authorize_id: authorize_result[i].authorize_id,
                  box_id: authorize_result[i].box_id,
                  station_id: authorize_result[i].station_id,
                },
                function (err, res) {}
              );

              var thread_authorize_log_data = {
                owner_id: authorize_result[i].owner_id,
                authorize_id: authorize_result[i].authorize_id,
                box_id: authorize_result[i].box_id,
                station_id: authorize_result[i].station_id,
                limit: authorize_result[i].limit,
                action: "revoke",
                authorize_time: currentTime,
              };

              new Worker("./thread_authorize_log.js", {
                workerData: thread_authorize_log_data,
              });
            }
          }
        );

        response.json("ok");
      }
    });
  });

  //check contact
  app.post("/checkContact", (request, response) => {
    var post_data = request.body;
    var user_id = post_data.user_id;
    var contact_id = post_data.contact_id;

    Contact.findOne({ user_id: user_id, contact_id: contact_id }, function (
      err,
      result
    ) {
      if (result != null) response.json({ message: true });
      else response.json({ message: false });
    });
  });

  //List authority person of user
  app.post("/list_authorize", (request, response, next) => {
    var post_data = request.body;
    var id_num = post_data.id_num;

    Authorize.find({ owner_id: id_num })
      .populate("box_id")
      .populate("station_id")
      .populate("authorize_id", { password: 0, salt: 0 })
      .exec(function (err, authorize) {
        if (err) throw err;
        response.json(authorize);
      });
  });

  //owner revokes
  app.post("/revoke_a_box", (request, response, next) => {
    var post_data = request.body;
    var authorize_id = post_data.authorize_id;
    var owner_id = post_data.owner_id;
    var box_id = post_data.box_id;
    var station_id = post_data.station_id;
    var currentTime = new Date().toString();

    Authorize.findOne(
      {
        $and: [
          { owner_id: owner_id },
          { authorize_id: authorize_id },
          { box_id: box_id },
          { station_id: station_id },
        ],
      },
      function (err, authorize_result) {
        if (authorize_result == null) {
          response.json("Box is already revoked");
          console.log("Box is already revoked");
        } else {
          var deleteJsonAuth = {
            owner_id: owner_id,
            authorize_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
          };

          Authorize.deleteOne(deleteJsonAuth, function (err, res) {});

          var deleteJsonUse = {
            user_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
          };

          Use.deleteOne(deleteJsonUse, function (err, res) {});

          response.json("ok");
          var thread_authorize_log_data = {
            owner_id: owner_id,
            authorize_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
            limit: authorize_result.limit,
            action: "revoke",
            authorize_time: currentTime,
          };

          new Worker("./thread_authorize_log.js", {
            workerData: thread_authorize_log_data,
          });
        }
      }
    );
  });

  //authority person rejects
  app.post("/reject_a_box", (request, response, next) => {
    var post_data = request.body;
    var authorize_id = post_data.authorize_id;

    var box_id = post_data.box_id;
    var station_id = post_data.station_id;

    Authorize.findOne(
      {
        $and: [
          { authorize_id: authorize_id },
          { box_id: box_id },
          { station_id: station_id },
        ],
      },
      function (err, authorize_result) {
        if (authorize_result == null) {
          response.json("Box is already rejected");
          console.log("Box is already rejected");
        } else {
          var deleteJsonAuth = {
            authorize_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
          };
          Authorize.deleteOne(deleteJsonAuth, function (err, res) {});

          var deleteJsonUse = {
            user_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
          };

          Use.deleteOne(deleteJsonUse, function (err, res) {});
          response.json("ok");
        }
      }
    );
  });

  //Onwer authorizes someone
  app.post("/auth_a_box", async (request, response, next) => {
    var post_data = request.body;
    var authorize_id = post_data.authorize_id;
    var owner_id = post_data.owner_id;
    var box_id = post_data.box_id;
    var station_id = post_data.station_id;
    var limit = post_data.limit;
    var start_time = post_data.start_time;
    var end_time = post_data.end_time;
    var currentTime = new Date().toString();

    Use.findOne(
      {
        $and: [
          { station_id: station_id },
          { user_id: authorize_id },
          { box_id: box_id },
        ],
      },
      async function (err, Use_result) {
        if (Use_result != null) {
          response.json("Box is already authorized");
          console.log("Box is already authorized");
        } else {
          var insertJsonUse = {
            user_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
            role: "auth",
            limit: limit,
            start_time: start_time,
            end_time: end_time,
          };
          Use(insertJsonUse).save();

          var insertJsonAuth = {
            authorize_id: authorize_id,
            owner_id: owner_id,
            box_id: box_id,
            station_id: station_id,
            limit: limit,
          };

          await Authorize(insertJsonAuth).save();

          Authorize.findOne(insertJsonAuth)
            .populate("box_id")
            .populate("station_id")
            .populate("owner_id")
            .exec(function (err, authorize_result) {
              io.sockets.emit("authorize", authorize_result);
            });

          response.json("authorize successfully");
          var thread_authorize_log_data = {
            owner_id: owner_id,
            authorize_id: authorize_id,
            box_id: box_id,
            station_id: station_id,
            limit: limit,
            action: "authorize",
            authorize_time: currentTime,
          };

          new Worker("./thread_authorize_log.js", {
            workerData: thread_authorize_log_data,
          });
        }
      }
    );
  });
};
