const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_open_log");
console.log("new open_log");

mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.open_log(
      workerData.id_num,
      workerData.box_id,
      workerData.station_id,
      workerData.currentTime
    );

    mongoose.disconnect();
  }
);
