var mongoose = require("mongoose");
var express = require("express");
var bodyParser = require("body-parser");
var circuitInteract = require("./circuitInteract");
var orderActivity = require("./orderActivity");
var authorizeActivity = require("./authorizeActivity");
var registerActivity = require("./registerActivity");
var blockchain = require("./blockchain");
var access = require("./access");
const { Worker } = require("worker_threads");

var url = "ADD YOUR DATABASE ADDRESS HERE!";

process.env.URL = url;
process.env.DATABASE = "dhd";
process.env.NODE_NAME = "main";

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var server = require("http").createServer(app);
server.listen(process.env.PORT || 3000);

var io;
io = require("socket.io").listen(server);

mongoose.connect(
  url,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  function (err, mongoose_result) {
    if (err) console.log("Unable to connect to the mongoDB server.Error", err);
    else {
      console.log(
        "Connected to MongoDB Server, WebService running on port 3000"
      );
      access.Internal(io, app);
      circuitInteract.hardware(io, app);
      circuitInteract.manipulate_box(io, app);
      orderActivity.Internal(io, app);
      authorizeActivity.Internal(io, app);
      registerActivity.Internal(io, app);

      blockchain.order_broadcast(io, app);
      new Worker("./thread_order_restart.js", {});

      blockchain.payment_broadcast(io, app);
      new Worker("./thread_payment_restart.js", {});

      blockchain.open_log_broadcast(io, app);
      new Worker("./thread_open_log_restart.js", {});

      blockchain.authorize_log_broadcast(io, app);
      new Worker("./thread_authorize_log_restart.js", {});
    }
  }
);
