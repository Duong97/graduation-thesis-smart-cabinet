var Cabinet = require("./models/cabinet");
var Station = require("./models/station");
var Use = require("./models/use");
const delay = require("delay");
var auxiliary = require("./auxiliary");

exports.Internal = (io, app) => {
  //Station_list_for_register
  app.post("/list_station", (request, response, next) => {
    Station.find({})
      .populate("price_id")
      .exec(function (err, result) {
        response.json(result);
      });
  });

  //Station_list_for_resgister base on search key
  app.post("/list_station_search_key", async (request, response, next) => {
    await delay(100);
    var post_data = request.body;

    var search_data = post_data.search_data;
    console.log(search_data);

    Station.find({
      $or: [
        { placename: { $regex: new RegExp("^" + search_data + ".*", "i") } },
        { address: { $regex: new RegExp("^" + search_data + ".*", "i") } },
      ],
    })
      .populate("price_id")
      .exec(function (err, result) {
        response.json(result);
      });
  });

  //Station_list_for_register near user (500m)
  app.post("/list_station_near_user", async (request, response, next) => {
    await delay(100);
    var post_data = request.body;

    var latitude = Number(post_data.latitude);
    var longitude = Number(post_data.longitude);
    console.log(`User latitude: ${latitude}`);
    console.log(`User longitude: ${longitude}`);

    var station_result = await Station.find({}).populate("price_id");
    var returnList = [];
    for (var i = 0; i < station_result.length; i++) {
      var distance = await auxiliary.calDistance(
        latitude,
        longitude,
        station_result[i].coordinate.latitude,
        station_result[i].coordinate.longitude
      );

      // console.log(
      //   `you are far away from station in ${station_result[i].placename} location ${station_result[i].location} no ${station_result[i].no} about ${distance}`
      // );

      if (distance <= 500) {
        await returnList.push(station_result[i]);
      }
    }

    response.json(returnList);
  });

  //Box_list_for_register
  app.post("/list_box", (request, response, next) => {
    var post_data = request.body;
    var station_id = post_data.station_id;

    //test sort query result
    Cabinet.find(
      {
        $and: [{ station_id: station_id }, { state: 0 }],
      },
      null,
      { sort: { _id: 1 } },
      function (err, cabinet) {
        response.json(cabinet);
      }
    );
  });

  //register_a_box
  app.post("/take_a_box", (request, response, next) => {
    var post_data = request.body;
    var id_num = post_data.id_num;
    var box_id = post_data.box_id;
    var box_no = post_data.box_no;
    var station_id = post_data.station_id;

    Use.findOne(
      {
        $and: [
          { station_id: station_id },
          { user_id: id_num },
          { box_id: box_id },
        ],
      },
      function (err, use) {
        if (use != null) {
          response.json("Box is already hired");
          console.log("Box is already hired");
        } else {
          var insertJson = {
            user_id: id_num,
            box_id: box_id,
            station_id: station_id,
            role: "own",
          };
          Use(insertJson).save(function (err) {
            response.json("Registration success");
            console.log("Registration success");
          });

          var updateJson = {
            _id: box_id,
            station_id: station_id,
          };

          var newvalues = { state: 1 };

          Cabinet.updateOne(updateJson, newvalues, function (err, cabinet) {
            console.log("Update success");
          });
        }
      }
    );
  });
};
