exports.calculatePeriod = (period_kind) => {
  var curentTime = new Date();
  var currentHour = curentTime.getHours();
  var currentMinute = curentTime.getMinutes();
  var currentDay = curentTime.getDate();
  var currentMonth = curentTime.getMonth() + 1;
  var currentYear = curentTime.getFullYear();

  var endHour;
  var endDay;
  var endMonth;
  var endYear;

  //Caculate period
  if (period_kind == "1") {
    endDay = currentDay;
    endMonth = currentMonth;
    endYear = currentYear;
    if (currentHour == 23) {
      endHour = 0;

      if ([1, 3, 5, 7, 8, 10, 12].includes(currentMonth)) {
        if (currentDay == 31) {
          endDay = 1;
          if (currentMonth == 12) {
            endMonth = 1;
            endYear = currentYear + 1;
          } else endMonth = currentMonth + 1;
        } else endDay = currentDay + 1;
      } else if ([4, 6, 9, 11].includes(currentMonth)) {
        if (currentDay == 30) {
          endDay = 1;
          endMonth = currentMonth + 1;
        } else endDay = currentDay + 1;
      } else {
        var lastDayFeb;
        if (
          (currentYear % 4 == 0 && currentYear % 100 != 0) ||
          currentYear % 400 == 0
        ) {
          lastDayFeb = 29;
        } else lastDayFeb = 28;
        if (currentDay == lastDayFeb) {
          endDay = 1;
          endMonth = currentMonth + 1;
        } else endDay = currentDay + 1;
      }
    } else {
      endHour = currentHour + 1;
    }
  }

  if (period_kind == "2") {
    endHour = currentHour;
    endMonth = currentMonth;
    endYear = currentYear;

    if ([1, 3, 5, 7, 8, 10, 12].includes(currentMonth)) {
      if (currentDay == 31) {
        endDay = 1;
        if (currentMonth == 12) {
          endMonth = 1;
          endYear = currentYear + 1;
        } else endMonth = currentMonth + 1;
      } else endDay = currentDay + 1;
    } else if ([4, 6, 9, 11].includes(currentMonth)) {
      if (currentDay == 30) {
        endDay = 1;
        endMonth = currentMonth + 1;
      } else endDay = currentDay + 1;
    } else {
      var lastDayFeb;
      if (
        (currentYear % 4 == 0 && currentYear % 100 != 0) ||
        currentYear % 400 == 0
      ) {
        lastDayFeb = 29;
      } else lastDayFeb = 28;
      if (currentDay == lastDayFeb) {
        endDay = 1;
        endMonth = currentMonth + 1;
      } else endDay = currentDay + 1;
    }
  }

  if (period_kind == "3") {
    endHour = currentHour;
    endDay = currentDay;
    endYear = currentYear;
    if (currentMonth == 12) {
      endMonth = 1;
      endYear = currentYear + 1;
    } else {
      endMonth = currentMonth + 1;
    }
  }

  var start_time =
    currentMonth +
    " " +
    currentDay +
    " " +
    currentYear +
    " " +
    currentHour +
    ":" +
    currentMinute;

  var end_time =
    endMonth +
    " " +
    endDay +
    " " +
    endYear +
    " " +
    endHour +
    ":" +
    currentMinute;

  var temp1 = new Date(start_time);
  var temp2 = new Date(end_time);

  var result = { start_time: start_time, end_time: end_time };
  return result;
};

exports.rebuild_order = async (token, rawOrderChain) => {
  var order_chain_unit = [];
  var connect_order_chain_unit = [];
  for (var j = 0; j < rawOrderChain.length; j++) {
    if (token == rawOrderChain[j].token) {
      order_chain_unit.push(rawOrderChain[j]);
    }
  }

  for (var i = 0; i < order_chain_unit.length; i++) {
    for (var j = i + 1; j < order_chain_unit.length; j++) {
      if (order_chain_unit[i].index > order_chain_unit[j].index) {
        var temp = order_chain_unit[i];
        order_chain_unit[i] = order_chain_unit[j];
        order_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < order_chain_unit.length; k++) {
    connect_order_chain_unit = connect_order_chain_unit.concat(
      order_chain_unit[k].chain
    );
  }
  return connect_order_chain_unit;
};

exports.rebuild_payment = async (token, rawPaymentChain) => {
  var payment_chain_unit = [];
  var connect_payment_chain_unit = [];
  for (var j = 0; j < rawPaymentChain.length; j++) {
    if (token == rawPaymentChain[j].token) {
      payment_chain_unit.push(rawPaymentChain[j]);
    }
  }

  for (var i = 0; i < payment_chain_unit.length; i++) {
    for (var j = i + 1; j < payment_chain_unit.length; j++) {
      if (payment_chain_unit[i].index > payment_chain_unit[j].index) {
        var temp = payment_chain_unit[i];
        payment_chain_unit[i] = payment_chain_unit[j];
        payment_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < payment_chain_unit.length; k++) {
    connect_payment_chain_unit = connect_payment_chain_unit.concat(
      payment_chain_unit[k].chain
    );
  }
  return connect_payment_chain_unit;
};

exports.rebuild_openLog = async (token, rawOpenLogChain) => {
  var open_log_chain_unit = [];
  var connect_open_log_chain_unit = [];
  for (var j = 0; j < rawOpenLogChain.length; j++) {
    if (token == rawOpenLogChain[j].token) {
      open_log_chain_unit.push(rawOpenLogChain[j]);
    }
  }

  for (var i = 0; i < open_log_chain_unit.length; i++) {
    for (var j = i + 1; j < open_log_chain_unit.length; j++) {
      if (open_log_chain_unit[i].index > open_log_chain_unit[j].index) {
        var temp = open_log_chain_unit[i];
        open_log_chain_unit[i] = open_log_chain_unit[j];
        open_log_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < open_log_chain_unit.length; k++) {
    connect_open_log_chain_unit = connect_open_log_chain_unit.concat(
      open_log_chain_unit[k].chain
    );
  }
  return connect_open_log_chain_unit;
};

exports.rebuild_authorizeLog = async (token, rawAuthorizeLogChain) => {
  var authorize_log_chain_unit = [];
  var connect_authorize_log_chain_unit = [];
  for (var j = 0; j < rawAuthorizeLogChain.length; j++) {
    if (token == rawAuthorizeLogChain[j].token) {
      authorize_log_chain_unit.push(rawAuthorizeLogChain[j]);
    }
  }

  for (var i = 0; i < authorize_log_chain_unit.length; i++) {
    for (var j = i + 1; j < authorize_log_chain_unit.length; j++) {
      if (
        authorize_log_chain_unit[i].index > authorize_log_chain_unit[j].index
      ) {
        var temp = authorize_log_chain_unit[i];
        authorize_log_chain_unit[i] = authorize_log_chain_unit[j];
        authorize_log_chain_unit[j] = temp;
      }
    }
  }

  for (var k = 0; k < authorize_log_chain_unit.length; k++) {
    connect_authorize_log_chain_unit = connect_authorize_log_chain_unit.concat(
      authorize_log_chain_unit[k].chain
    );
  }
  return connect_authorize_log_chain_unit;
};

exports.calDistance = async (la1, lo1, la2, lo2) => {
  R = 6371e3;
  dLat = (la2 - la1) * (Math.PI / 180);
  dLon = (lo2 - lo1) * (Math.PI / 180);
  la1ToRad = la1 * (Math.PI / 180);
  la2ToRad = la2 * (Math.PI / 180);
  a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(la1ToRad) *
      Math.cos(la2ToRad) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  d = R * c;

  return d;
};
