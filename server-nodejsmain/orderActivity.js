var crypto = require("crypto");
var auxiliary = require("./auxiliary");
const { v1: uuidv1 } = require("uuid");
const { Worker } = require("worker_threads");

var Own = require("./models/own");
var Use = require("./models/use");
var Cabinet = require("./models/cabinet");

exports.Internal = (io, app) => {
  app.post("/checkOwn", (request, response, next) => {
    var post_data = request.body;
    var id_num = post_data.id_num;
    var box_id = post_data.box_id;
    var station_id = post_data.station_id;
    console.log("hello");

    Own.findOne(
      {
        $and: [
          { station_id: station_id },
          { user_id: id_num },
          { box_id: box_id },
        ],
      },
      function (err, own) {
        if (own != null) {
          response.json("1");
          console.log("Box is already hired");
        } else {
          response.json("0");
          console.log("Box is not hired yet");
        }
      }
    );
  });

  //payMoMo
  app.post("/payMoMo", async (request, response, next) => {
    console.log("payMoMo");
    var post_data = request.body;
    var id_num = post_data.id_num;
    var box_id = post_data.box_id;
    var station_id = post_data.station_id;
    var period_kind = post_data.period_kind;
    var partnerCode = post_data.partnerCode;
    var customerNumber = post_data.customerNumber;
    var token = post_data.token;
    var amount = post_data.amount; //request in database
    var order_time = new Date().toString();

    RandExp = require("randexp");
    randexp = new RandExp(/^dhd-[0-9a-zA-Z]{20}/);
    var partnerRefId = randexp.gen();

    //ADD YOUR PUBLIC KEY HERE! FOLOW THE FORMAT BELOW (separate the key into each segment)
    var publicKeySource =
      "-----BEGIN PUBLIC KEY-----" +
      "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" +
      "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" +
      "-----END PUBLIC KEY-----";

    var samplejsonString = {
      partnerCode: partnerCode,
      partnerRefId: partnerRefId,
      amount: Number(amount),
    };

    const NodeRSA = require("node-rsa");

    const publicKey = await new NodeRSA(publicKeySource, {
      encryptionScheme: "pkcs1",
    });
    var hash = await publicKey.encrypt(samplejsonString, "base64");

    const requestMoMo = require("request");

    requestMoMo.post(
      "https://test-payment.momo.vn/pay/app",
      {
        json: {
          partnerCode: partnerCode,
          customerNumber: customerNumber,
          partnerRefId: partnerRefId,
          appData: token,
          //appData: null,
          hash: hash,
          description: "Thanh toan cho don hang " + partnerRefId + " qua MoMo",
          version: 2,
          payType: 3,
        },
      },
      (error, res, body) => {
        if (error) {
          console.error(error);
          return;
        }

        console.log("inside request");
        console.log(JSON.stringify(body));
        response.json(body);

        if (body.status == 0) {
          var period_result = auxiliary.calculatePeriod(period_kind);
          var start_time = period_result.start_time;
          var end_time = period_result.end_time;

          var thread_order_data = {
            partnerRefId: partnerRefId,
            id_num: id_num,
            box_id: box_id,
            station_id: station_id,
            start_time: start_time,
            end_time: end_time,
            order_time: order_time,
          };

          new Worker("./thread_order.js", { workerData: thread_order_data });

          var pay_info = {
            customerNumber: customerNumber,
          };
          var pay_date = new Date().toString();

          var thread_payment_data = {
            pay_service: "MoMo",
            pay_num: body.transid,
            pay_info: pay_info,
            pay_date: pay_date,
            cost: amount,
            order_id: partnerRefId,
          };
          new Worker("./thread_payment.js", {
            workerData: thread_payment_data,
          });

          var insertJsonUse = {
            user_id: id_num,
            box_id: box_id,
            station_id: station_id,
            role: "own",
            limit: "unknown",
            start_time: start_time,
            end_time: end_time,
          };

          Use(insertJsonUse).save();

          var insertJsonOwn = {
            user_id: id_num,
            box_id: box_id,
            station_id: station_id,
            start_time: start_time,
            end_time: end_time,
          };

          Own(insertJsonOwn).save();

          var updateJson = {
            _id: box_id,
            station_id: station_id,
          };
          var newvalues = { state: 1 };

          Cabinet.updateOne(updateJson, newvalues, function (err, res) {});

          Cabinet.findOne(
            {
              $and: [{ station_id: station_id }, { _id: box_id }],
            },
            function (err, cabinet) {
              var sendinfoJson = {
                box_id: cabinet._id,
                box_no: cabinet.no,
                station_id: cabinet.station_id,
                start_time: start_time,
                end_time: end_time,
              };
              io.sockets.emit("info_update", sendinfoJson);
            }
          );

          //Transfer money to bussiness wallet
          var serectkey = "ADD YOUR SECRET KEY HERE!";
          var requestType = "capture";
          var requestId = uuidv1();
          var momoTransId = body.transid;
          var rawSignature =
            "partnerCode=" +
            partnerCode +
            "&partnerRefId=" +
            partnerRefId +
            "&requestType=" +
            requestType +
            "&requestId=" +
            requestId +
            "&momoTransId=" +
            momoTransId;
          var signature = crypto
            .createHmac("sha256", serectkey)
            .update(rawSignature)
            .digest("hex");
          const committMoMo = require("request");
          committMoMo.post(
            "https://test-payment.momo.vn/pay/confirm",
            {
              json: {
                partnerCode: partnerCode,
                partnerRefId: partnerRefId,
                requestType: requestType,
                requestId: requestId,
                momoTransId: momoTransId,
                signature: signature,
              },
            },
            (committMoMo_error, committMoMo_res, committMoMo_body) => {
              if (committMoMo_error) {
                console.error(committMoMo_error);
                return;
              }
              console.log(JSON.stringify(committMoMo_body));
            }
          );
        }
      }
    );
  });

  //payDefault
  app.post("/payDefault", async (request, response, next) => {
    console.log("payDefault");
    var post_data = request.body;
    var id_num = post_data.id_num;
    var box_id = post_data.box_id;
    var station_id = post_data.station_id;
    var period_kind = post_data.period_kind;
    var amount = post_data.amount;
    var order_time = new Date().toString();

    RandExp = require("randexp");
    randexp = new RandExp(/^dhd-[0-9a-zA-Z]{20}/);
    var partnerRefId = randexp.gen();

    var customerNumber = "0911111111";

    if (true) {
      response.json({ message: "OK" });
      var period_result = auxiliary.calculatePeriod(period_kind);
      var start_time = period_result.start_time;
      var end_time = period_result.end_time;

      var thread_order_data = {
        partnerRefId: partnerRefId,
        id_num: id_num,
        box_id: box_id,
        station_id: station_id,
        start_time: start_time,
        end_time: end_time,
        order_time: order_time,
      };

      new Worker("./thread_order.js", { workerData: thread_order_data });

      var pay_info = {
        customerNumber: customerNumber,
      };
      var pay_date = new Date().toString();
      var transidGen = new RandExp(/[0-9]{10}/);
      var transid = transidGen.gen();
      var thread_payment_data = {
        pay_service: "Default",
        pay_num: transid.toString(),
        pay_info: pay_info,
        pay_date: pay_date,
        cost: amount,
        order_id: partnerRefId,
      };
      new Worker("./thread_payment.js", {
        workerData: thread_payment_data,
      });

      var insertJsonUse = {
        user_id: id_num,
        box_id: box_id,
        station_id: station_id,
        role: "own",
        limit: "unknown",
        start_time: start_time,
        end_time: end_time,
      };

      Use(insertJsonUse).save();

      var insertJsonOwn = {
        user_id: id_num,
        box_id: box_id,
        station_id: station_id,
        start_time: start_time,
        end_time: end_time,
      };

      Own(insertJsonOwn).save();

      var updateJson = {
        _id: box_id,
        station_id: station_id,
      };
      var newvalues = { state: 1 };

      Cabinet.updateOne(updateJson, newvalues, function (err, res) {});

      Cabinet.findOne(
        {
          $and: [{ station_id: station_id }, { _id: box_id }],
        },
        function (err, cabinet) {
          var sendinfoJson = {
            box_id: cabinet._id,
            box_no: cabinet.no,
            station_id: cabinet.station_id,
            start_time: start_time,
            end_time: end_time,
          };
          io.sockets.emit("info_update", sendinfoJson);
        }
      );
    }
  });
};
