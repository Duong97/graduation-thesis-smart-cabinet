const { Worker } = require("worker_threads");

var list_node = [];
var list_user = [];
var Order_pendingchain = require("./models/order_pendingchain");
var Payment_pendingchain = require("./models/payment_pendingchain");
var Open_log_pendingchain = require("./models/open_log_pendingchain");
var Authorize_log_pendingchain = require("./models/authorize_log_pendingchain");

//for order table
var order_response = 0;
process.env.ORDER_BROADCAST = true;
process.env.ORDER_MINING = false;
process.env.ORDER_CHAINS_COMPARISON = false;
process.env.ORDER_REPLACE = false;
process.env.ORDER_RESPONSE_RESET = true;

//for payment table
var payment_response = 0;
process.env.PAYMENT_BROADCAST = true;
process.env.PAYMENT_MINING = false;
process.env.PAYMENT_CHAINS_COMPARISON = false;
process.env.PAYMENT_REPLACE = false;
process.env.PAYMENT_RESPONSE_RESET = true;

//for open_log table
var open_log_response = 0;
process.env.OPEN_LOG_BROADCAST = true;
process.env.OPEN_LOG_MINING = false;
process.env.OPEN_LOG_CHAINS_COMPARISON = false;
process.env.OPEN_LOG_REPLACE = false;
process.env.OPEN_LOG_RESPONSE_RESET = true;

//for authorize_log table
var authorize_log_response = 0;
process.env.AUTHORIZE_LOG_BROADCAST = true;
process.env.AUTHORIZE_LOG_MINING = false;
process.env.AUTHORIZE_LOG_CHAINS_COMPARISON = false;
process.env.AUTHORIZE_LOG_REPLACE = false;
process.env.AUTHORIZE_LOG_RESPONSE_RESET = true;

exports.Internal = (io, socket) => {
  var socket_id = socket.id;

  socket.on("Node", function (data) {
    console.log(`Name of socket id ${data.socket_id}: ${data.name}`);
    list_node.push(data);
    console.log(`Number connected node is ${list_node.length}`);
  });

  socket.on("disconnect", function (data) {
    console.log(`Socket disconnected, ID: ${socket_id}`);
    var found = list_node.findIndex((node) => node.socket_id == socket_id);
    if (found != -1) {
      list_node.splice(found, 1);
      console.log(`Number connected node is ${list_node.length}`);
    }
    if (list_node.length == 0) {
      process.env.ORDER_BROADCAST = true;
      process.env.PAYMENT_BROADCAST = true;
      process.env.OPEN_LOG_BROADCAST = true;
      process.env.AUTHORIZE_LOG_BROADCAST = true;
    }
  });

  //order_broadcast
  socket.on("order_chains_comparison", function (data) {
    if (process.env.ORDER_RESPONSE_RESET === "true") {
      process.env.ORDER_RESPONSE_RESET = false;
      order_response = 0;
    }

    order_response += 1;
    console.log(
      `order_chains_comparison with current number response ${order_response} from ${data.from}`
    );

    if (order_response >= list_node.length) {
      process.env.ORDER_BROADCAST = true;
    } else {
      process.env.ORDER_BROADCAST = false;
    }
  });

  socket.on("new_order_chain_from_node", async function (data) {
    if (process.env.ORDER_RESPONSE_RESET === "true") {
      process.env.ORDER_RESPONSE_RESET = false;
      order_response = 0;
    }

    order_response += 1;
    console.log(
      `new_order_chains_from_node with current number response ${order_response} from ${data.from}`
    );

    if (order_response >= list_node.length) {
      process.env.ORDER_BROADCAST = true;
    } else {
      process.env.ORDER_BROADCAST = false;
    }

    var transfer = {
      chain: data.chain,
      from: data.from,
      token: data.token,
    };

    io.sockets.emit("new_order_chain_from_main", transfer);

    var chain = data.chain;
    segmentLength = 500;
    initLength = chain.length;
    index = 0;

    while (true) {
      if (initLength > segmentLength) {
        initLength -= segmentLength;
        var cutChain = await chain.slice(0, segmentLength);
        chain = await chain.slice(segmentLength);

        var transfer = {
          chain: cutChain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Order_pendingchain(transfer).save();
        index += 1;
      } else {
        var transfer = {
          chain: chain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Order_pendingchain(transfer).save();
        index += 1;
        break;
      }
    }

    if (process.env.ORDER_CHAINS_COMPARISON === "false") {
      process.env.ORDER_CHAINS_COMPARISON = true;
      process.env.ORDER_REPLACE = false;
      new Worker("./thread_update_orderChain.js", {});
    }
  });

  //payment_broadcast
  socket.on("payment_chains_comparison", function (data) {
    if (process.env.PAYMENT_RESPONSE_RESET === "true") {
      process.env.PAYMENT_RESPONSE_RESET = false;
      payment_response = 0;
    }

    payment_response += 1;
    console.log(
      `payment_chains_comparison with current number response ${payment_response} from ${data.from}`
    );

    if (payment_response >= list_node.length) {
      process.env.PAYMENT_BROADCAST = true;
    } else {
      process.env.PAYMENT_BROADCAST = false;
    }
  });

  socket.on("new_payment_chain_from_node", async function (data) {
    if (process.env.PAYMENT_RESPONSE_RESET === "true") {
      process.env.PAYMENT_RESPONSE_RESET = false;
      payment_response = 0;
    }

    payment_response += 1;
    console.log(
      `new_payment_chains_from_node with current number response ${payment_response} from ${data.from}`
    );

    if (payment_response >= list_node.length) {
      process.env.PAYMENT_BROADCAST = true;
    } else {
      process.env.PAYMENT_BROADCAST = false;
    }

    var transfer = {
      chain: data.chain,
      from: data.from,
      token: data.token,
    };

    io.sockets.emit("new_payment_chain_from_main", transfer);

    var chain = data.chain;
    segmentLength = 500;
    initLength = chain.length;
    index = 0;

    while (true) {
      if (initLength > segmentLength) {
        initLength -= segmentLength;
        var cutChain = await chain.slice(0, segmentLength);
        chain = await chain.slice(segmentLength);

        var transfer = {
          chain: cutChain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Payment_pendingchain(transfer).save();
        index += 1;
      } else {
        var transfer = {
          chain: chain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Payment_pendingchain(transfer).save();
        index += 1;
        break;
      }
    }

    if (process.env.PAYMENT_CHAINS_COMPARISON === "false") {
      process.env.PAYMENT_CHAINS_COMPARISON = true;
      process.env.PAYMENT_REPLACE = false;
      new Worker("./thread_update_paymentChain.js", {});
    }
  });

  //open_log_broadcast
  socket.on("open_log_chains_comparison", function (data) {
    if (process.env.OPEN_LOG_RESPONSE_RESET === "true") {
      process.env.OPEN_LOG_RESPONSE_RESET = false;
      open_log_response = 0;
    }

    open_log_response += 1;
    console.log(
      `open_log_chains_comparison with current number response ${open_log_response} from ${data.from}`
    );

    if (open_log_response >= list_node.length) {
      process.env.OPEN_LOG_BROADCAST = true;
    } else {
      process.env.OPEN_LOG_BROADCAST = false;
    }
  });

  socket.on("new_open_log_chain_from_node", async function (data) {
    if (process.env.OPEN_LOG_RESPONSE_RESET === "true") {
      process.env.OPEN_LOG_RESPONSE_RESET = false;
      open_log_response = 0;
    }

    open_log_response += 1;
    console.log(
      `new_open_log_chain_from_node with current number response ${open_log_response} from ${data.from}`
    );

    if (open_log_response >= list_node.length) {
      process.env.OPEN_LOG_BROADCAST = true;
    } else {
      process.env.OPEN_LOG_BROADCAST = false;
    }

    var transfer = {
      chain: data.chain,
      from: data.from,
      token: data.token,
    };

    io.sockets.emit("new_open_log_chain_from_main", transfer);

    var chain = data.chain;
    segmentLength = 500;
    initLength = chain.length;
    index = 0;

    while (true) {
      if (initLength > segmentLength) {
        initLength -= segmentLength;
        var cutChain = await chain.slice(0, segmentLength);
        chain = await chain.slice(segmentLength);

        var transfer = {
          chain: cutChain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Open_log_pendingchain(transfer).save();
        index += 1;
      } else {
        var transfer = {
          chain: chain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Open_log_pendingchain(transfer).save();
        index += 1;
        break;
      }
    }

    if (process.env.OPEN_LOG_CHAINS_COMPARISON === "false") {
      process.env.OPEN_LOG_CHAINS_COMPARISON = true;
      process.env.OPEN_LOG_REPLACE = false;
      new Worker("./thread_update_openlogChain.js", {});
    }
  });

  //authorize_log_broadcast
  socket.on("authorize_log_chains_comparison", function (data) {
    if (process.env.AUTHORIZE_LOG_RESPONSE_RESET === "true") {
      process.env.AUTHORIZE_LOG_RESPONSE_RESET = false;
      authorize_log_response = 0;
    }

    authorize_log_response += 1;
    console.log(
      `authorize_log_chains_comparison with current number response ${authorize_log_response} from ${data.from}`
    );

    if (authorize_log_response >= list_node.length) {
      process.env.AUTHORIZE_LOG_BROADCAST = true;
    } else {
      process.env.AUTHORIZE_LOG_BROADCAST = false;
    }
  });

  socket.on("new_authorize_log_chain_from_node", async function (data) {
    if (process.env.AUTHORIZE_LOG_RESPONSE_RESET === "true") {
      process.env.AUTHORIZE_LOG_RESPONSE_RESET = false;
      authorize_log_response = 0;
    }

    authorize_log_response += 1;
    console.log(
      `new_authorize_log_chains_from_node with current number response ${authorize_log_response} from ${data.from}`
    );

    if (authorize_log_response >= list_node.length) {
      process.env.AUTHORIZE_LOG_BROADCAST = true;
    } else {
      process.env.AUTHORIZE_LOG_BROADCAST = false;
    }

    var transfer = {
      chain: data.chain,
      from: data.from,
      token: data.token,
    };

    io.sockets.emit("new_authorize_log_chain_from_main", transfer);

    var chain = data.chain;
    segmentLength = 500;
    initLength = chain.length;
    index = 0;

    while (true) {
      if (initLength > segmentLength) {
        initLength -= segmentLength;
        var cutChain = await chain.slice(0, segmentLength);
        chain = await chain.slice(segmentLength);

        var transfer = {
          chain: cutChain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Authorize_log_pendingchain(transfer).save();
        index += 1;
      } else {
        var transfer = {
          chain: chain,
          from: data.from,
          token: data.token,
          index: index,
        };

        await Authorize_log_pendingchain(transfer).save();
        index += 1;
        break;
      }
    }

    if (process.env.AUTHORIZE_LOG_CHAINS_COMPARISON === "false") {
      process.env.AUTHORIZE_LOG_CHAINS_COMPARISON = true;
      process.env.AUTHORIZE_LOG_REPLACE = false;
      new Worker("./thread_update_authorizelogChain.js", {});
    }
  });
};
