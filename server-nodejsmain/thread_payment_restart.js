const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_payment_restart");

mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.payment_restart();
    mongoose.disconnect();
  }
);
