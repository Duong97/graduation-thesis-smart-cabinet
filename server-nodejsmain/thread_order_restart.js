const { workerData, parentPort } = require("worker_threads");
var blockchain = require("./blockchain");
var mongoose = require("mongoose");

console.log("thread_order_restart");

mongoose.connect(
  process.env.URL,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    dbName: process.env.DATABASE,
  },
  async function (err, mongoose_result) {
    await blockchain.order_restart();
    mongoose.disconnect();
  }
);
